package com.ladestitute.zarrowsandstuff.items.gear.sets.snowquill;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class SnowquillTunicItem extends ArmorItem {
    public SnowquillTunicItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Lined with molted Rito feathers, this tunic was made by Rito artisans for Hylians visiting cold climates. The feathers are stacked in each layer to retain body heat."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}
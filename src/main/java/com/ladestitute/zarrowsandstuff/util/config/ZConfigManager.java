package com.ladestitute.zarrowsandstuff.util.config;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ZConfigManager {
    private static final ZConfigManager INSTANCE;

    private static final ForgeConfigSpec SPEC;

    private static final Path CONFIG_PATH =
            Paths.get("config", ZArrowsMain.MOD_ID + ".toml");

    static {
        Pair<ZConfigManager, ForgeConfigSpec> specPair =
                new ForgeConfigSpec.Builder().configure(ZConfigManager::new);
        INSTANCE = specPair.getLeft();
        SPEC = specPair.getRight();
        CommentedFileConfig config = CommentedFileConfig.builder(CONFIG_PATH)
                .sync()
                .autoreload()
                .writingMode(WritingMode.REPLACE)
                .build();
        config.load();
        config.save();
        SPEC.setConfig(config);
    }

    //General
    private final BooleanValue alwaysShowStamina;
    private final BooleanValue alwaysShowTemp;
    private final BooleanValue selfShockArrowImmunity;
    private final BooleanValue experimentalFeatures;

    //Vanilla-like
    private final ForgeConfigSpec.IntValue ArmorSetCount;
    public final ForgeConfigSpec.IntValue modFoodStackSize;
    private final BooleanValue addoptionaltrades;
    private final BooleanValue generatemodplants;
    private final BooleanValue icearrowsfrostedice;
    private final BooleanValue usecryostone;

    //Combat
    private final BooleanValue allowfirearrowdot;
    public final ForgeConfigSpec.IntValue damagescaling;
    private final ForgeConfigSpec.IntValue foodCooldown;
    private final BooleanValue oneshotblazes;

    // Whether to enable the temperature system

    private final BooleanValue enableTemperatureSystem;
    private final BooleanValue allowTierOneBiomes;
    private final BooleanValue resistpotioncooldown;

    public final ForgeConfigSpec.IntValue tempDamage;
    private final ForgeConfigSpec.IntValue campfireDuration;

    private final BooleanValue allowFoodHealing;

    // Whether campfires reduce freezing/overheating gradually or completely when in range
    private final BooleanValue gradualCampfireEffect;

    // The y-level of mountains that are treated as tier-two cold biomes
    public final ForgeConfigSpec.IntValue mountainYLevel;

    // The default temperature-timer threshold for freezing/overheating
    private final ForgeConfigSpec.IntValue defaultTempTimerThreshold;

    // Whether to enable ore deposits spawning underground
    private final BooleanValue undergroundOreDeposits;

    // Whether ore deposits are renewable, if enabled, they will become depleted instead of vanishing
    private final BooleanValue renewableOreDeposits;

    /**
     * Implementation of Singleton design pattern, which allows only one
     * instance of this class to be created.
     */
    private ZConfigManager(ForgeConfigSpec.Builder configSpecBuilder) {

        //General
        alwaysShowStamina = configSpecBuilder
                .translation("zarrowsandstuff.configGui.alwaysshowstamina.title")
                .define("alwaysshowstamina", false);
        alwaysShowTemp = configSpecBuilder
                .translation("zarrowsandstuff.configGui.alwaysshowstemp.title")
                .define("alwaysshowtemp", true);
        selfShockArrowImmunity = configSpecBuilder
                .translation("zarrowsandstuff.configGui.selfshock.title")
                .define("selfshock", false);
        experimentalFeatures = configSpecBuilder
                .translation("zarrowsandstuff.configGui.experimental.title")
                .define("experimental", false);

        //Vanilla-like
        ArmorSetCount = configSpecBuilder
                .translation("zarrowsandstuff.configGui.armorsetcount.title")
                .defineInRange("armorsetcount", 3, 3, 4);
        modFoodStackSize = configSpecBuilder
                .translation("zarrowsandstuff.configGui.modfoodstacksize.title")
                .defineInRange("modfoodstacksize", 1, 1, 16);
        resistpotioncooldown = configSpecBuilder
                .translation("zarrowsandstuff.configGui.resistpotioncooldown.title")
                .define("resistpotioncooldown", true);
        addoptionaltrades = configSpecBuilder
                .translation("zarrowsandstuff.configGui.addoptionaltrades.title")
                .define("addoptionaltrades", true);
        generatemodplants = configSpecBuilder
                .translation("zarrowsandstuff.configGui.generatemodplants.title")
                .define("generatemodplants", true);
        icearrowsfrostedice = configSpecBuilder
                .translation("zarrowsandstuff.configGui.icearrowsfrostedice.title")
                .define("icearrowsfrostedice", false);
        usecryostone = configSpecBuilder
                .translation("zarrowsandstuff.configGui.usecryostone.title")
                .define("usecryostone", true);

        //Combat
        allowfirearrowdot = configSpecBuilder
                .translation("zarrowsandstuff.configGui.allowfirearrowdot.title")
                .define("allowfirearrowdot", false);
        damagescaling = configSpecBuilder
                .translation("zarrowsandstuff.configGui.damagescaling.title")
                .defineInRange("damagescaling", 1, 0, 2);
        foodCooldown = configSpecBuilder
                .translation("zarrowsandstuff.configGui.foodcooldown.title")
                .defineInRange("foodcooldown", 600, 0, 1200);
        oneshotblazes = configSpecBuilder
                .translation("zarrowsandstuff.configGui.oneshotblazes.title")
                .define("oneshotblazes", false);

        enableTemperatureSystem = configSpecBuilder
                .translation("zarrowsandstuff.configGui.enabletemperaturesystem.title")
                .define("enabletemperaturesystem", true);
        allowTierOneBiomes = configSpecBuilder
                .translation("zarrowsandstuff.configGui.allowtieronebiomes.title")
                .define("allowtieronebiomes", true);
        campfireDuration = configSpecBuilder
                .translation("zarrowsandstuff.configGui.campfireduration.title")
                .defineInRange("campfireduration", 1200, 600, 3000);
        tempDamage = configSpecBuilder
                .translation("zarrowsandstuff.configGui.tempdamage.title")
                .defineInRange("tempdamage", 2, 1, 10);
        allowFoodHealing = configSpecBuilder
                .translation("zarrowsandstuff.configGui.allowfoodhealing.title")
                .define("allowfoodhealing", true);
        gradualCampfireEffect = configSpecBuilder
                .translation("zarrowsandstuff.configGui.gradualcampfireeffect.title")
                .define("gradualcampfireeffect", true);
        mountainYLevel = configSpecBuilder
                .translation("zarrowsandstuff.configGui.mountainylevel.title")
                .defineInRange("mountainylevel", 100, 87, 123);
        defaultTempTimerThreshold = configSpecBuilder
                .translation("zarrowsandstuff.configGui.defaulttemptimerthreshold.title")
                .defineInRange("defaulttemptimerthreshold", 200, 100, 400);
        undergroundOreDeposits = configSpecBuilder
                .translation("zarrowsandstuff.configGui.undergroundore.title")
                .define("undergroundore", true);
        renewableOreDeposits = configSpecBuilder
                .translation("zarrowsandstuff.configGui.renewableore.title")
                .define("renewableore", true);

    }

    public static ZConfigManager getInstance() {
        return INSTANCE;
    }
    // Query Operations

    //General
    public boolean selfShockArrowImmunity() { return selfShockArrowImmunity.get(); }
    public boolean experimentalFeatures() { return experimentalFeatures.get(); }

    //Vanilla-like
    public int armorSetCount() { return ArmorSetCount.get(); }
    public boolean alwaysshowStamina() { return alwaysShowStamina.get(); }
    public boolean alwaysshowTemp() { return alwaysShowTemp.get(); }
    public int modfoodstacksize() { return modFoodStackSize.get(); }
    public boolean resistPotioncooldown() { return resistpotioncooldown.get(); }
    public boolean addoptionaltrades() { return addoptionaltrades.get(); }
    public boolean generatemodplants() { return generatemodplants.get(); }
    public boolean icearrowsfrostedice() { return icearrowsfrostedice.get(); }
    public boolean useCryostone() { return usecryostone.get(); }

    //Combat
    public int damageScaling() { return damagescaling.get(); }
    public boolean allowFireArrowDOT() { return allowfirearrowdot.get(); }
    public int foodCooldown() { return foodCooldown.get(); }
    public boolean oneshotblazes() { return oneshotblazes.get(); }

    //Temp system
    public boolean allowfoodhealing() { return allowFoodHealing.get(); }
    public int campfireDuration() { return campfireDuration.get(); }
    public int tempDamage() { return tempDamage.get(); }
    public boolean enableTemperatureSystem() { return enableTemperatureSystem.get(); }
    public boolean allowTierOneBiomes() { return allowTierOneBiomes.get(); }
    public int mountainYLevel() { return mountainYLevel.get(); }
    public int defaultTempTimerThreshold() { return defaultTempTimerThreshold.get(); }

    //Gen
    public boolean undergroundOreDeposits() {
        return undergroundOreDeposits.get();
    }
    public boolean renewableOreDeposits() {
        return renewableOreDeposits.get();
    }

    ////////
    // Modification Operations
    ////////

    //General
    public void changeSelfShockArrowImmunity(boolean newValue) {
        selfShockArrowImmunity.set(newValue);
    }
    public void changeExperimentalFeatures(boolean newValue) {
        experimentalFeatures.set(newValue);
    }

    //Vanilla-like
    public void changeArmorSetCount(int newValue) {ArmorSetCount.set(newValue);}
    public void changeModFoodStackSize(int newValue) {modFoodStackSize.set(newValue);}
    public void changeAddOptionalTrades(boolean newValue) {addoptionaltrades.set(newValue);}
    public void changeresistpotioncooldown(boolean newValue) {resistpotioncooldown.set(newValue);}
    public void changeGeneratemodplants(boolean newValue) {generatemodplants.set(newValue);}
    public void changeIceArrowsFrostedIce(boolean newValue) {icearrowsfrostedice.set(newValue); }
    public void changeUsecryostone(boolean newValue) {usecryostone.set(newValue); }

    //Combat
    public void changeAllowFireArrowDOT(boolean newValue) {allowfirearrowdot.set(newValue);}
    public void changeDamagescaling(int newValue) {damagescaling.set(newValue);}
    public void changeFoodCooldown(int newValue) {foodCooldown.set(newValue);}
    public void changeOneshotblazes(boolean newValue) {oneshotblazes.set(newValue);}

    public void enableTemperatureSystem(boolean newValue) {enableTemperatureSystem.set(newValue);}
    public void changeAllowTierOneBiomes(boolean newValue) {allowTierOneBiomes.set(newValue);}

    public void showStamina(boolean newValue) {alwaysShowStamina.set(newValue);}
    public void changeshowTemp(boolean newValue) {alwaysShowTemp.set(newValue);}

    public void changeCampfireDuration(int newValue) {campfireDuration.set(newValue);}

    public void setTempDamage(int newValue) {tempDamage.set(newValue);}

    public void changeallowFoodHealing(boolean newValue) {allowFoodHealing.set(newValue);}

    public void changeGradualCampfireEffect(boolean newValue) {gradualCampfireEffect.set(newValue);}

    public void changeMountainYLevel(int newValue) {mountainYLevel.set(newValue);}

    public void changeDefaultTempTimerThreshold(int newValue) {defaultTempTimerThreshold.set(newValue);}

    public void changeUndergroundOreDeposits(boolean newValue) {
        undergroundOreDeposits.set(newValue);
    }

    public void changeRenewableOreDeposits(boolean newValue) {
        renewableOreDeposits.set(newValue);
    }

    public void save() {
        SPEC.save();
    }
}

package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.entities.arrows.EntityAncientArrow;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityIceArrow;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.BlazeEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMobDropsHandler {

    @SubscribeEvent
    public void blazekiller(LivingHurtEvent event) {
        Entity sourceEntity = event.getSource().getImmediateSource();

        if (ZConfigManager.getInstance().oneshotblazes() && sourceEntity instanceof EntityIceArrow && event.getEntityLiving() instanceof BlazeEntity) {
            event.setAmount(10000F);
            //  PlayerEntity player = (PlayerEntity)event.getEntityLiving();
        }
    }

    @SubscribeEvent
    public void onMobDrops(LivingDropsEvent event) {
        Entity sourceEntity = event.getSource().getImmediateSource();
        if (sourceEntity instanceof EntityAncientArrow) {
            event.getDrops().clear();
          //  PlayerEntity player = (PlayerEntity)event.getEntityLiving();
        }
    }

    @SubscribeEvent
            public void topazthudner(LivingDamageEvent event)
    {
        ItemStack helmstack = event.getEntityLiving().getItemStackFromSlot(EquipmentSlotType.HEAD);
        if(event.getSource().damageType == DamageSource.LIGHTNING_BOLT.damageType &&
        event.getEntityLiving() instanceof PlayerEntity && event.getEntityLiving().hasItemInSlot(EquipmentSlotType.HEAD))
        if(helmstack.getItem() == ItemInit.TOPAZ_EARRINGS.get())
        {
            event.setAmount(event.getAmount()/2);
        }
        if(helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get())
        {
            event.setAmount(event.getAmount()/4);
        }
    }
}


package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.EffectType;
import net.minecraft.potion.Potion;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class EffectInit {

    public static final DeferredRegister<Effect> EFFECTS = DeferredRegister.create(ForgeRegistries.POTIONS,
            ZArrowsMain.MOD_ID);

    public static final RegistryObject<Effect> COLD_RESIST_EFFECT =
            EFFECTS.register("cold_resist", () -> new ColdResistEffect(EffectType.NEUTRAL, 0x90FFFD));
    public static final RegistryObject<Effect> HEAT_RESIST_EFFECT =
            EFFECTS.register("heat_resist", () -> new HeatResistEffect(EffectType.NEUTRAL, 0xFFB600));

    public static final DeferredRegister<Potion> POTIONS = DeferredRegister.create(ForgeRegistries.POTION_TYPES, ZArrowsMain.MOD_ID);

    public static final RegistryObject<Potion> COLD_RESIST_POTION = POTIONS.register("cold_resist", () ->
            new Potion(new EffectInstance(COLD_RESIST_EFFECT.get(), 9000)));
    public static final RegistryObject<Potion> HEAT_RESIST_POTION = POTIONS.register("heat_resist", () ->
            new Potion(new EffectInstance(HEAT_RESIST_EFFECT.get(), 9000)));
    //Unused, for a planned 1/10 'critical brew' chance which doubles the duration
    //public static final RegistryObject<Potion> COLD_RESIST_POTION_DOUBLE = POTIONS.register("cold_resist_double", () ->
            //new Potion(new EffectInstance(COLD_RESIST_EFFECT.get(), 18000)));
    //public static final RegistryObject<Potion> HEAT_RESIST_POTION_DOUBLE = POTIONS.register("heat_resist_double", () ->
            //new Potion(new EffectInstance(HEAT_RESIST_EFFECT.get(), 18000)));

    //
    public static class ColdResistEffect extends Effect {

        public ColdResistEffect(EffectType typeIn, int liquidColorIn) {
            super(typeIn, liquidColorIn);
        }

    }
    //
    public static class HeatResistEffect extends Effect {

        public HeatResistEffect(EffectType typeIn, int liquidColorIn) {
            super(typeIn, liquidColorIn);
        }

    }
    //
    public static class FreezingEffect extends Effect {

        public FreezingEffect(EffectType typeIn, int liquidColorIn) {
            super(typeIn, liquidColorIn);
        }

    }
    //
    public static class OverheatingEffect extends Effect {

        public OverheatingEffect(EffectType typeIn, int liquidColorIn) {
            super(typeIn, liquidColorIn);
        }

    }
}

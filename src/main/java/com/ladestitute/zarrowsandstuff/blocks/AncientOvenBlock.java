package com.ladestitute.zarrowsandstuff.blocks;

import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.util.KeyboardUtil;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.Tags;
import net.minecraftforge.items.ItemHandlerHelper;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

import net.minecraft.block.AbstractBlock.Properties;

public class AncientOvenBlock extends Block {
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public AncientOvenBlock(Properties properties) {
        super(properties.setLightLevel((state) -> {
            return 10;
        }));
        this.setDefaultState(this.stateContainer.getBaseState().with(FACING, Direction.NORTH));
       // this.setDefaultState(defaultBlockState);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(FACING);
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(FACING)));
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

    @Override
    public void addInformation (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new
                StringTextComponent("An advanced furnace made from ancient wreckage and a partially broken Guidance Stone. Its capability to manufacture weaponry is beyond anything seen in this world."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        //Our item list

        //Ancient Arrow components
        ItemStack arrow = Items.ARROW.getDefaultInstance();
        Block stone = Blocks.STONE;
        ItemStack screw = ItemInit.ANCIENT_SCREW.get().getDefaultInstance();
        ItemStack shaft = ItemInit.ANCIENT_SHAFT.get().getDefaultInstance();
        ItemStack ancientarrow = ItemInit.ANCIENT_ARROW.get().getDefaultInstance();

        //Essences
        ItemStack raw_fire = ItemInit.RAW_FIRE_ESSENCE.get().getDefaultInstance();
        ItemStack raw_ice = ItemInit.RAW_ICE_ESSENCE.get().getDefaultInstance();
        ItemStack raw_thunder = ItemInit.RAW_THUNDER_ESSENCE.get().getDefaultInstance();
        ItemStack fire = ItemInit.FIRE_ESSENCE.get().getDefaultInstance();
        ItemStack ice = ItemInit.ICE_ESSENCE.get().getDefaultInstance();
        ItemStack thunder = ItemInit.THUNDER_ESSENCE.get().getDefaultInstance();
        ItemStack topazearringsupgraded = ItemInit.UPGRADED_TOPAZ_EARRINGS.get().getDefaultInstance();
        ItemStack rubyupgraded = ItemInit.UPGRADED_RUBY_CIRCLET.get().getDefaultInstance();
        ItemStack sapphireupgraded = ItemInit.UPGRADED_SAPPHIRE_CIRCLET.get().getDefaultInstance();

        ItemStack handstack = player.getItemStackFromSlot(EquipmentSlotType.MAINHAND);

        if(ZConfigManager.getInstance().experimentalFeatures()) {
            if (player.inventory.hasItemStack(raw_fire) && player.experienceLevel >= 1)
                for (ItemStack raw_fire_essence : player.inventory.mainInventory)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.RAW_FIRE_ESSENCE.get() && raw_fire_essence.getItem() == ItemInit.RAW_FIRE_ESSENCE.get()) {
                        player.sendStatusMessage(ITextComponent.getTextComponentOrEmpty("Refine a fire essence? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingEnter()) {
                            raw_fire_essence.shrink(1);
                            ItemHandlerHelper.giveItemToPlayer(player, fire);
                            player.addExperienceLevel(-1);
                        }
                    }

            if (player.inventory.hasItemStack(raw_ice) && player.experienceLevel >= 1)
                for (ItemStack raw_ice_essence : player.inventory.mainInventory)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.RAW_ICE_ESSENCE.get() && raw_ice_essence.getItem() == ItemInit.RAW_ICE_ESSENCE.get()) {
                        player.sendStatusMessage(ITextComponent.getTextComponentOrEmpty("Refine an ice essence? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingEnter()) {
                            raw_ice_essence.shrink(1);
                            ItemHandlerHelper.giveItemToPlayer(player, ice);
                            player.addExperienceLevel(-1);
                        }
                    }

            if (player.inventory.hasItemStack(raw_thunder) && player.experienceLevel >= 1)
                for (ItemStack raw_thunder_essence : player.inventory.mainInventory)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.RAW_THUNDER_ESSENCE.get() && raw_thunder_essence.getItem() == ItemInit.RAW_THUNDER_ESSENCE.get()) {
                        player.sendStatusMessage(ITextComponent.getTextComponentOrEmpty("Refine a thunder essence? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingEnter()) {
                            raw_thunder_essence.shrink(1);
                            ItemHandlerHelper.giveItemToPlayer(player, thunder);
                            player.addExperienceLevel(-1);
                        }
                    }

            if (player.inventory.hasItemStack(thunder) && player.experienceLevel >= 1)
                for (ItemStack thunder1 : player.inventory.mainInventory)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.TOPAZ_EARRINGS.get() && thunder1.getItem() == ItemInit.THUNDER_ESSENCE.get()) {
                        player.sendStatusMessage(ITextComponent.getTextComponentOrEmpty("Upgrade Topaz Earrings? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingEnter()) {
                            for (ItemStack topaz : player.inventory.mainInventory)
                                if (topaz.getItem() == ItemInit.TOPAZ_EARRINGS.get())
                                {
                                    topaz.shrink(1);
                                }
                            for (ItemStack material : player.inventory.mainInventory)
                                if (material.getItem() == ItemInit.THUNDER_ESSENCE.get())
                                {
                                    material.shrink(1);
                                }
                            ItemHandlerHelper.giveItemToPlayer(player, topazearringsupgraded);
                            player.addExperienceLevel(-1);
                        }
                    }

            if (player.inventory.hasItemStack(fire) && player.experienceLevel >= 1)
                for (ItemStack check : player.inventory.mainInventory)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.RUBY_CIRCLET.get() && check.getItem() == ItemInit.FIRE_ESSENCE.get()) {
                        player.sendStatusMessage(ITextComponent.getTextComponentOrEmpty("Upgrade Ruby Circlet? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingEnter()) {
                            for (ItemStack upgrade : player.inventory.mainInventory)
                                if (upgrade.getItem() == ItemInit.RUBY_CIRCLET.get())
                                {
                                    upgrade.shrink(1);
                                }
                            for (ItemStack material : player.inventory.mainInventory)
                                if (material.getItem() == ItemInit.FIRE_ESSENCE.get())
                                {
                                    material.shrink(1);
                                }
                            ItemHandlerHelper.giveItemToPlayer(player, rubyupgraded);
                            player.addExperienceLevel(-1);
                        }
                    }

            if (player.inventory.hasItemStack(ice) && player.experienceLevel >= 1)
                for (ItemStack check : player.inventory.mainInventory)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.SAPPHIRE_CIRCLET.get() && check.getItem() == ItemInit.ICE_ESSENCE.get()) {
                        player.sendStatusMessage(ITextComponent.getTextComponentOrEmpty("Upgrade Sapphire Circlet? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingEnter()) {
                            for (ItemStack upgrade : player.inventory.mainInventory)
                                if (upgrade.getItem() == ItemInit.SAPPHIRE_CIRCLET.get())
                                {
                                    upgrade.shrink(1);
                                }
                            for (ItemStack material : player.inventory.mainInventory)
                                if (material.getItem() == ItemInit.ICE_ESSENCE.get())
                                {
                                    material.shrink(1);
                                }
                            ItemHandlerHelper.giveItemToPlayer(player, sapphireupgraded);
                            player.addExperienceLevel(-1);
                        }
                    }
        }

        if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                handstack.getItem() == Items.ARROW && player.inventory.hasItemStack(arrow) && player.inventory.hasItemStack(shaft) &&
                player.inventory.hasItemStack(screw) && player.experienceLevel >= 2)
        {
            player.sendStatusMessage(ITextComponent.getTextComponentOrEmpty("Craft an ancient arrow? Hold Enter and right-click the block again to confirm."), true);
            if(KeyboardUtil.isHoldingEnter())
            {
                //Check for the screws
                for (ItemStack screws : player.inventory.mainInventory)
                    if (screws.getItem() == ItemInit.ANCIENT_SCREW.get())
                    {
                        screws.shrink(1);
                    }
                //Check for the arrows
                for (ItemStack arrows : player.inventory.mainInventory)
                    if (arrows.getItem() == Items.ARROW)
                    {
                        arrows.shrink(1);
                    }
                //Check for the shafts
                for (ItemStack shafts : player.inventory.mainInventory)
                    if (shafts.getItem() == ItemInit.ANCIENT_SHAFT.get())
                    {
                        shafts.shrink(1);
                    }
                player.addExperienceLevel(-2);
                ItemHandlerHelper.giveItemToPlayer(player, ancientarrow);
            }
                }
        //Check if the player does not meet requirements
        if (player.experienceLevel == 0)
        {
            player.sendStatusMessage(ITextComponent.getTextComponentOrEmpty("You do not have the required experience cost."), true);
        }
                return ActionResultType.SUCCESS;

            }
        }


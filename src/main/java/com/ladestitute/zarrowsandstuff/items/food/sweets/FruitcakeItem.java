package com.ladestitute.zarrowsandstuff.items.food.sweets;

import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class FruitcakeItem extends Item {

    public FruitcakeItem(Item.Properties properties)
    {
        super(properties.maxStackSize(ZConfigManager.getInstance().modFoodStackSize.get()));
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.EAT;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 10;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Making ample use of fruits found all over Hyrule, this cake is a must for celebrations."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}
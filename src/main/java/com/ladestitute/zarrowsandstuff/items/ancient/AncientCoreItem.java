package com.ladestitute.zarrowsandstuff.items.ancient;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.item.Items;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

import net.minecraft.item.Item.Properties;

public class AncientCoreItem extends Item {
    public AncientCoreItem(Properties properties)
    {
        super(properties.maxStackSize(64));
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        PlayerEntity player = Minecraft.getInstance().player;
        ItemStack mainstack = player.getItemStackFromSlot(EquipmentSlotType.MAINHAND);
        if(player.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SWORD)
        {
            //do stuff
        }
        return super.onItemUse(context);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("This crystal was made using lost technology. At one time it was the power source for ancient machines. This item is very valuable to researchers."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
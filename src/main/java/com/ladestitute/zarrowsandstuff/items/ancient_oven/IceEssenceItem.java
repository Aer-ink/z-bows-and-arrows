package com.ladestitute.zarrowsandstuff.items.ancient_oven;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

import net.minecraft.item.Item.Properties;

public class IceEssenceItem extends Item {
    public IceEssenceItem(Properties properties)
    {
        super(properties.maxStackSize(64));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Pure essence of ice. It is highly refined and stabilized from the unpure element contained within its respective gem."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
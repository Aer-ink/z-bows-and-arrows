package com.ladestitute.zarrowsandstuff.blocks.natural.plants;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;

import java.util.List;

public class StamellaShroomBlock extends Block {
    public StamellaShroomBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void addInformation (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("A green mushroom that grows near trees in the forest. It's chock-full of natural energy. Cook it to release its stamina-restoration properties."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}


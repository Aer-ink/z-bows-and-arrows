package com.ladestitute.zarrowsandstuff.registries;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;

public class PropertiesInit {

    public static final AbstractBlock.Properties ANCIENTTECH = AbstractBlock.Properties.create(Material.ROCK)
            .hardnessAndResistance(5f, 5f)
            .sound(SoundType.STONE)
            .harvestLevel(1)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool();

    public static final AbstractBlock.Properties WRECKAGE = AbstractBlock.Properties.create(Material.IRON)
            .hardnessAndResistance(4f, 3.25f)
            .sound(SoundType.METAL)
            .harvestLevel(0)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool();

    public static final AbstractBlock.Properties SNOWSTONE = AbstractBlock.Properties.create(Material.ROCK)
            .hardnessAndResistance(0.8f, 0.8f)
            .sound(SoundType.STONE)
            .harvestLevel(0)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool();

    public static final AbstractBlock.Properties ORE_DEPOSIT = AbstractBlock.Properties.create(Material.ROCK)
            .hardnessAndResistance(3f, 3f)
            .sound(SoundType.STONE)
            .harvestLevel(1)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool()
            .notSolid();

    public static final AbstractBlock.Properties SPICY_PEPPER = AbstractBlock.Properties.create(Material.PLANTS)
            .hardnessAndResistance(0.0f, 0.0F)
            .sound(SoundType.PLANT)
            .doesNotBlockMovement()
            .tickRandomly();

    public static final AbstractBlock.Properties H_STEM = AbstractBlock.Properties.create(Material.PLANTS)
            .hardnessAndResistance(0f, 0f)
            .sound(SoundType.PLANT)
            .doesNotBlockMovement()
            .tickRandomly();

    public static final AbstractBlock.Properties HYDROMELON = AbstractBlock.Properties.create(Material.PLANTS)
            .hardnessAndResistance(1f, 1f)
            .harvestLevel(0)
            .sound(SoundType.PLANT)
            .harvestTool(ToolType.AXE);

}

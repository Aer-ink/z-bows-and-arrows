package com.ladestitute.zarrowsandstuff.client.render.arrows;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityBombArrow;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BombArrowRender extends ArrowRenderer<EntityBombArrow> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows/bomb_arrow.png");

    public BombArrowRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getEntityTexture(EntityBombArrow entity) {
        return TEXTURE;
    }


}

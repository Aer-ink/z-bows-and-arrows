package com.ladestitute.zarrowsandstuff.items.gear.bows;

import com.ladestitute.zarrowsandstuff.util.ZToolMaterials;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class SoldiersBowItem extends BowItem {

    public SoldiersBowItem(ZToolMaterials toolmaterial, Properties builder) {
        super(builder.maxStackSize(1).maxDamage((556)));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A bow designed for armed conflict. Inflicts more damage than a civilian bow, but it will still burn if it touches fire."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}


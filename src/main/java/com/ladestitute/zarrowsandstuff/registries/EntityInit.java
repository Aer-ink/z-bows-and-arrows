package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.arrows.*;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class EntityInit {

    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, ZArrowsMain.MOD_ID);

    //Arrows
    public static final RegistryObject<EntityType<EntityFireArrow>> FIRE_ARROW = ENTITIES.register("fire_arrow",
            () -> EntityType.Builder.<EntityFireArrow>create(EntityFireArrow::new, EntityClassification.MISC).size(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityIceArrow>> ICE_ARROW = ENTITIES.register("ice_arrow",
            () -> EntityType.Builder.<EntityIceArrow>create(EntityIceArrow::new, EntityClassification.MISC).size(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityShockArrow>> SHOCK_ARROW = ENTITIES.register("shock_arrow",
            () -> EntityType.Builder.<EntityShockArrow>create(EntityShockArrow::new, EntityClassification.MISC).size(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityBombArrow>> BOMB_ARROW = ENTITIES.register("bomb_arrow",
            () -> EntityType.Builder.<EntityBombArrow>create(EntityBombArrow::new, EntityClassification.MISC).size(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityAncientArrow>> ANCIENT_ARROW = ENTITIES.register("ancient_arrow",
            () -> EntityType.Builder.<EntityAncientArrow>create(EntityAncientArrow::new, EntityClassification.MISC).size(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityLightArrow>> LIGHT_ARROW = ENTITIES.register("light_arrow",
            () -> EntityType.Builder.<EntityLightArrow>create(EntityLightArrow::new, EntityClassification.MISC).size(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
}

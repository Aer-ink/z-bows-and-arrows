package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.items.ancient.AncientCoreItem;
import com.ladestitute.zarrowsandstuff.items.ancient.AncientFragmentItem;
import com.ladestitute.zarrowsandstuff.items.ancient.AncientScrewItem;
import com.ladestitute.zarrowsandstuff.items.ancient.AncientShaftItem;
import com.ladestitute.zarrowsandstuff.items.ancient_oven.*;
import com.ladestitute.zarrowsandstuff.items.arrows.*;
import com.ladestitute.zarrowsandstuff.items.food.*;
import com.ladestitute.zarrowsandstuff.items.gear.*;
import com.ladestitute.zarrowsandstuff.items.gear.bows.GoldenBowItem;
import com.ladestitute.zarrowsandstuff.items.gear.bows.SilverBowItem;
import com.ladestitute.zarrowsandstuff.items.gear.bows.SoldiersBowItem;
import com.ladestitute.zarrowsandstuff.items.gear.bows.TravelersBowItem;
import com.ladestitute.zarrowsandstuff.items.gear.sets.desert_voe.DesertVoeBootsItem;
import com.ladestitute.zarrowsandstuff.items.gear.sets.desert_voe.DesertVoeHeadbandItem;
import com.ladestitute.zarrowsandstuff.items.gear.sets.desert_voe.DesertVoeSpaulderItem;
import com.ladestitute.zarrowsandstuff.items.gear.sets.desert_voe.DesertVoeTrousersItem;
import com.ladestitute.zarrowsandstuff.items.gear.sets.snowquill.SnowquillBootsItem;
import com.ladestitute.zarrowsandstuff.items.gear.sets.snowquill.SnowquillHeaddressItem;
import com.ladestitute.zarrowsandstuff.items.gear.sets.snowquill.SnowquillTrousersItem;
import com.ladestitute.zarrowsandstuff.items.gear.sets.snowquill.SnowquillTunicItem;
import com.ladestitute.zarrowsandstuff.items.natural.*;
import com.ladestitute.zarrowsandstuff.util.ZArmorMaterials;
import com.ladestitute.zarrowsandstuff.util.ZToolMaterials;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ItemInit {

    //Registry initialization
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            ZArrowsMain.MOD_ID);

    //Ancient Materials
    public static final RegistryObject<Item> ANCIENT_SCREW = ITEMS.register("ancient_screw",
            () -> new AncientScrewItem(new Item.Properties().group(ZArrowsMain.ANCIENT_TAB)));
    public static final RegistryObject<Item> ANCIENT_SHAFT = ITEMS.register("ancient_shaft",
            () -> new AncientShaftItem(new Item.Properties().group(ZArrowsMain.ANCIENT_TAB)));
    public static final RegistryObject<Item> ANCIENT_CORE = ITEMS.register("ancient_core",
            () -> new AncientCoreItem(new Item.Properties().group(ZArrowsMain.ANCIENT_TAB)));
    public static final RegistryObject<Item> ANCIENT_FRAGMENT = ITEMS.register("ancient_fragment",
            () -> new AncientFragmentItem(new Item.Properties().group(ZArrowsMain.ANCIENT_TAB)));

    public static final RegistryObject<Item> SPICY_PEPPER_SEED = ITEMS.register("spicy_pepper_seed",
            () -> new SpicyPepperSeedItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> HYDROMELON_SEED = ITEMS.register("hydromelon_seed",
            () -> new HydromelonSeedItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));

    //Resource foods
    public static final RegistryObject<Item> SPICY_PEPPER = ITEMS.register("spicy_pepper",
            () -> new SpicyPepperItem(new Item.Properties().food(new Food.Builder().hunger(1)
                    .saturation(0.6f).setAlwaysEdible()
                    .build()).group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> HYDROMELON_FRUIT = ITEMS.register("hydromelon_fruit",
            () -> new HydromelonFruitItem(new Item.Properties().food(new Food.Builder().hunger(2)
                    .saturation(1f).setAlwaysEdible()
                    .build()).group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> STAMELLA_SHROOM = ITEMS.register("stamella_shroom",
            () -> new StamellaShroomItem(new Item.Properties().food(new Food.Builder().hunger(1)
                    .saturation(0.6f).setAlwaysEdible()
                    .build()).group(ZArrowsMain.RESOURCES_TAB)));

    //Natural
    public static final RegistryObject<Item> RUBY = ITEMS.register("ruby",
            () -> new RubyItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> SAPPHIRE = ITEMS.register("sapphire",
            () -> new SapphireItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> TOPAZ = ITEMS.register("topaz",
            () -> new TopazItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> OPAL = ITEMS.register("opal",
            () -> new OpalItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> AMBER = ITEMS.register("amber",
            () -> new AmberItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));

    //Ancient Oven products
    public static final RegistryObject<Item> RAW_FIRE_ESSENCE = ITEMS.register("raw_fire_essence",
            () -> new RawFireEssenceItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> RAW_ICE_ESSENCE = ITEMS.register("raw_ice_essence",
            () -> new RawIceEssenceItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> RAW_THUNDER_ESSENCE = ITEMS.register("raw_thunder_essence",
            () -> new RawThunderEssenceItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> FIRE_ESSENCE = ITEMS.register("fire_essence",
            () -> new FireEssenceItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> ICE_ESSENCE = ITEMS.register("ice_essence",
            () -> new IceEssenceItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));
    public static final RegistryObject<Item> THUNDER_ESSENCE = ITEMS.register("thunder_essence",
            () -> new ThunderEssenceItem(new Item.Properties().group(ZArrowsMain.RESOURCES_TAB)));

    //Gear
    public static final RegistryObject<ArmorItem> OPAL_EARRINGS = ITEMS.register("opal_earrings",
            () -> new OpalEarringsItem(ZArmorMaterials.OPAL_EARRINGS, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ACCESSORIES_TAB)));
    public static final RegistryObject<ArmorItem> AMBER_EARRINGS = ITEMS.register("amber_earrings",
            () -> new AmberEarringsItem(ZArmorMaterials.AMBER_EARRINGS, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ACCESSORIES_TAB)));
    public static final RegistryObject<ArmorItem> TOPAZ_EARRINGS = ITEMS.register("topaz_earrings",
            () -> new TopazEarringsItem(ZArmorMaterials.TOPAZ_EARRINGS, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ACCESSORIES_TAB)));
    public static final RegistryObject<ArmorItem> RUBY_CIRCLET = ITEMS.register("ruby_circlet",
            () -> new RubyCircletItem(ZArmorMaterials.RUBY_CIRCLET, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ACCESSORIES_TAB)));
    public static final RegistryObject<ArmorItem> SAPPHIRE_CIRCLET = ITEMS.register("sapphire_circlet",
            () -> new SapphireCircletItem(ZArmorMaterials.SAPPHIRE_CIRCLET, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ACCESSORIES_TAB)));
    public static final RegistryObject<ArmorItem> UPGRADED_TOPAZ_EARRINGS = ITEMS.register("upgraded_topaz_earrings",
            () -> new TopazEarringsItem(ZArmorMaterials.UPGRADED_TOPAZ_EARRINGS, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ACCESSORIES_TAB)));
    public static final RegistryObject<ArmorItem> UPGRADED_RUBY_CIRCLET = ITEMS.register("upgraded_ruby_circlet",
            () -> new RubyCircletItem(ZArmorMaterials.UPGRADED_RUBY_CIRCLET, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ACCESSORIES_TAB)));
    public static final RegistryObject<ArmorItem> UPGRADED_SAPPHIRE_CIRCLET = ITEMS.register("upgraded_sapphire_circlet",
            () -> new SapphireCircletItem(ZArmorMaterials.UPGRADED_SAPPHIRE_CIRCLET, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ACCESSORIES_TAB)));

    //Bows
    public static final RegistryObject<BowItem> TRAVELERS_BOW = ITEMS.register("travelers_bow",
            () -> new TravelersBowItem(ZToolMaterials.TRAVELERS,
                    new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    public static final RegistryObject<BowItem> SOLDERS_BOW = ITEMS.register("soldiers_bow",
            () -> new SoldiersBowItem(ZToolMaterials.SOLDIERS,
                    new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    public static final RegistryObject<BowItem> SILVER_BOW = ITEMS.register("silver_bow",
            () -> new SilverBowItem(ZToolMaterials.SILVER,
                    new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    public static final RegistryObject<BowItem> GOLDEN_BOW = ITEMS.register("golden_bow",
            () -> new GoldenBowItem(ZToolMaterials.GOLDEN,
                    new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    //Arrows
    public static final RegistryObject<Item> FIRE_ARROW = ITEMS.register("fire_arrow",
            () -> new FireArrowItem(new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    public static final RegistryObject<Item> ICE_ARROW = ITEMS.register("ice_arrow",
            () -> new IceArrowItem(new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    public static final RegistryObject<Item> SHOCK_ARROW = ITEMS.register("shock_arrow",
            () -> new ShockArrowItem(new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    public static final RegistryObject<Item> BOMB_ARROW = ITEMS.register("bomb_arrow",
            () -> new BombArrowItem(new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    public static final RegistryObject<Item> ANCIENT_ARROW = ITEMS.register("ancient_arrow",
            () -> new AncientArrowItem(new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));
    public static final RegistryObject<Item> LIGHT_ARROW = ITEMS.register("light_arrow",
            () -> new LightArrowItem(new Item.Properties().group(ZArrowsMain.ARROWS_TAB)));

     ///Snowquill Set
    public static final RegistryObject<ArmorItem> SNOWQUILL_HEADDRESS = ITEMS.register("snowquill_headdress",
            () -> new SnowquillHeaddressItem(ZArmorMaterials.SNOWQUILL, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ARMOR_TAB)));
    public static final RegistryObject<ArmorItem> SNOWQUILL_TUNIC = ITEMS.register("snowquill_tunic",
            () -> new SnowquillTunicItem(ZArmorMaterials.SNOWQUILL, EquipmentSlotType.CHEST,
                    new Item.Properties().group(ZArrowsMain.ARMOR_TAB)));
    public static final RegistryObject<ArmorItem> SNOWQUILL_TROUSERS = ITEMS.register("snowquill_trousers",
            () -> new SnowquillTrousersItem(ZArmorMaterials.SNOWQUILL, EquipmentSlotType.LEGS,
                    new Item.Properties().group(ZArrowsMain.ARMOR_TAB)));
    public static final RegistryObject<ArmorItem> SNOWQUILL_BOOTS = ITEMS.register("snowquill_boots",
            () -> new SnowquillBootsItem(ZArmorMaterials.SNOWQUILL, EquipmentSlotType.FEET,
                    new Item.Properties().group(ZArrowsMain.ARMOR_TAB)));
     ///Gerudo Voe Set
    public static final RegistryObject<ArmorItem> DESERT_VOE_HEADBAND = ITEMS.register("desert_voe_headband",
            () -> new DesertVoeHeadbandItem(ZArmorMaterials.DESERT_VOE, EquipmentSlotType.HEAD,
                    new Item.Properties().group(ZArrowsMain.ARMOR_TAB)));
    public static final RegistryObject<ArmorItem> DESERT_VOE_SPAULDER = ITEMS.register("desert_voe_spaulder",
            () -> new DesertVoeSpaulderItem(ZArmorMaterials.DESERT_VOE, EquipmentSlotType.CHEST,
                    new Item.Properties().group(ZArrowsMain.ARMOR_TAB)));
    public static final RegistryObject<ArmorItem> DESERT_VOE_TROUSERS = ITEMS.register("desert_voe_trousers",
            () -> new DesertVoeTrousersItem(ZArmorMaterials.DESERT_VOE, EquipmentSlotType.LEGS,
                    new Item.Properties().group(ZArrowsMain.ARMOR_TAB)));
    public static final RegistryObject<ArmorItem> DESERT_VOE_BOOTS = ITEMS.register("desert_voe_boots",
            () -> new DesertVoeBootsItem(ZArmorMaterials.DESERT_VOE, EquipmentSlotType.FEET,
                    new Item.Properties().group(ZArrowsMain.ARMOR_TAB)));

    //Itemblocks

    public static final RegistryObject<Item> ANCIENT_OVEN_BLOCK = ITEMS.register("ancient_oven",
            () -> new BlockItem(SpecialBlocksInit.ANCIENT_OVEN.get(), new Item.Properties().group(ZArrowsMain.ANCIENTTECH_TAB)));
    public static final RegistryObject<Item> ANCIENT_CRYOSTONE_BLOCK = ITEMS.register("ancient_cryostone",
            () -> new BlockItem(SpecialBlocksInit.ANCIENT_CRYOSTONE.get(), new Item.Properties().group(ZArrowsMain.ANCIENTTECH_TAB)));
}

package com.ladestitute.zarrowsandstuff.util.events.tempsystem;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class ZHotBiomesHandler {
    //Todo: implement onplayerclone capability-handler for the nether in v0.3
    //Temperature system values
    public int overheattimer;

    //Possible timer extending modifiers
    private final int circletmodifier = 6000;

    @SubscribeEvent
    public void cooldownhandling(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;

        World world = player.world;

        player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
            if (h.getHeatResistCooldown() < 0) {
                h.setHeatResistCooldown(0);
            }

            if (h.getHeatResistCooldown() > 0) {
                h.subtractHeatResistCooldown(1);
            }
        });
    }

    @SubscribeEvent
    public void campfireradius(TickEvent.PlayerTickEvent event) {
        if (ZConfigManager.getInstance().enableTemperatureSystem()) {
            LivingEntity player = event.player;
            World world = player.world;
            BlockPos pos = new BlockPos(player.getPosition());

            Effect heatresisteffect = EffectInit.HEAT_RESIST_EFFECT.get();
            EffectInstance heatresist = player.getActivePotionEffect(heatresisteffect);

            //Cardinal directions one block from player
            BlockPos posWest = player.getPosition().west();
            BlockState blockStateWest = player.world.getBlockState(posWest);
            Block w = blockStateWest.getBlock(); //West
            BlockPos posEast = player.getPosition().east();
            BlockState blockStateEast = player.world.getBlockState(posEast);
            Block e = blockStateEast.getBlock(); //East
            BlockPos posNorth = player.getPosition().north();
            BlockState blockStateNorth = player.world.getBlockState(posNorth);
            Block n = blockStateNorth.getBlock(); //North
            BlockPos posSouth = player.getPosition().south();
            BlockState blockStateSouth = player.world.getBlockState(posSouth);
            Block s = blockStateSouth.getBlock(); //South
            //Diagonal directions one block from player
            BlockPos posSW = player.getPosition().south().west();
            BlockState blockStateSW = player.world.getBlockState(posSW);
            Block sw = blockStateSW.getBlock(); //SW
            BlockPos posSE = player.getPosition().south().east();
            BlockState blockStateSE = player.world.getBlockState(posSE);
            Block se = blockStateSE.getBlock(); //SE
            BlockPos posNW = player.getPosition().north().west();
            BlockState blockStateNW = player.world.getBlockState(posNW);
            Block nw = blockStateNW.getBlock(); //NW
            BlockPos posNE = player.getPosition().north().east();
            BlockState blockStateNE = player.world.getBlockState(posNE);
            Block ne = blockStateNE.getBlock(); //NE
            //Cardinal directions two blocks from player
            BlockPos posWestWest = player.getPosition().west().west();
            BlockState blockStateWestWest = player.world.getBlockState(posWestWest);
            Block ww = blockStateWestWest.getBlock(); //West x2
            BlockPos posEastEast = player.getPosition().east().east();
            BlockState blockStateEastEast = player.world.getBlockState(posEastEast);
            Block ee = blockStateEastEast.getBlock(); //East x2
            BlockPos posNorthNorth = player.getPosition().north().north();
            BlockState blockStateNorthNorth = player.world.getBlockState(posNorthNorth);
            Block nn = blockStateNorthNorth.getBlock(); //North x2
            BlockPos posSouthSouth = player.getPosition().south().south();
            BlockState blockStateSouthSouth = player.world.getBlockState(posSouthSouth);
            Block ss = blockStateSouthSouth.getBlock(); //South x2
            //Diagonal directions two blocks from the player
            BlockPos posNorthNorthWest = player.getPosition().north().north().west();
            BlockState blockStateNorthNorthWest = player.world.getBlockState(posNorthNorthWest);
            Block nnw = blockStateNorthNorthWest.getBlock(); //nw x2
            BlockPos posNorthNorthEast = player.getPosition().north().north().east();
            BlockState blockStateNorthNorthEast = player.world.getBlockState(posNorthNorthEast);
            Block nne = blockStateNorthNorthEast.getBlock(); //ne x2
            BlockPos posSouthSouthWest = player.getPosition().south().south().west();
            BlockState blockStateSouthSouthWest = player.world.getBlockState(posSouthSouthWest);
            Block ssw = blockStateSouthSouthWest.getBlock(); //sw x2
            BlockPos posSouthSouthEast = player.getPosition().south().south().east();
            BlockState blockStateSouthSouthEast = player.world.getBlockState(posSouthSouthEast);
            Block sse = blockStateSouthSouthEast.getBlock(); //ne x2
            //Corners, outmost
            BlockPos posNorthNorthWestCorner = player.getPosition().north().north().west().west();
            BlockState blockStateNorthNorthWestCorner = player.world.getBlockState(posNorthNorthWestCorner);
            Block c1 = blockStateNorthNorthWestCorner.getBlock(); //nw x2
            BlockPos posNorthNorthEastCorner = player.getPosition().north().north().east().east();
            BlockState blockStateNorthNorthEastCorner = player.world.getBlockState(posNorthNorthEastCorner);
            Block c2 = blockStateNorthNorthEastCorner.getBlock(); //ne x2
            BlockPos posSouthSouthWestCorner = player.getPosition().south().south().west().west();
            BlockState blockStateSouthSouthWestCorner = player.world.getBlockState(posSouthSouthWestCorner);
            Block c3 = blockStateSouthSouthWestCorner.getBlock(); //sw x2
            BlockPos posSouthSouthEastCorner = player.getPosition().south().south().east().east();
            BlockState blockStateSouthSouthEastCorner = player.world.getBlockState(posSouthSouthEastCorner);
            Block c4 = blockStateSouthSouthEastCorner.getBlock(); //ne x2

            BlockPos posDown = player.getPosition().down();
            BlockState blockStateDown = player.world.getBlockState(posDown);
            Block d = blockStateDown.getBlock();
            BlockPos posDownLeft = player.getPosition().down().west();
            BlockState blockStateDownLeft = player.world.getBlockState(posDownLeft);
            Block dw = blockStateDownLeft.getBlock();
            BlockPos posDownRight = player.getPosition().down().east();
            BlockState blockStateDownRight = player.world.getBlockState(posDownRight);
            Block de = blockStateDownRight.getBlock();
            BlockPos posDownUp = player.getPosition().down().north();
            BlockState blockStateDownUp = player.world.getBlockState(posDownUp);
            Block dn = blockStateDownUp.getBlock();
            BlockPos posDownsouth = player.getPosition().down().south();
            BlockState blockStateDownSouth = player.world.getBlockState(posDownsouth);
            Block ds = blockStateDownSouth.getBlock();
            BlockPos posDownNW = player.getPosition().down().north().west();
            BlockState blockStateDownNW = player.world.getBlockState(posDownNW);
            Block dnw = blockStateDownNW.getBlock();
            BlockPos posDownNE = player.getPosition().down().north().east();
            BlockState blockStateDownNE = player.world.getBlockState(posDownNE);
            Block dne = blockStateDownNE.getBlock();
            BlockPos posDownSW = player.getPosition().down().south().west();
            BlockState blockStateDownSW = player.world.getBlockState(posDownSW);
            Block dsw = blockStateDownSW.getBlock();
            BlockPos posDownSE = player.getPosition().down().south().east();
            BlockState blockStateDownSE = player.world.getBlockState(posDownSE);
            Block dse = blockStateDownSE.getBlock();

            if (d == Blocks.CAMPFIRE || n == Blocks.CAMPFIRE || s == Blocks.CAMPFIRE ||
                    w == Blocks.CAMPFIRE || e == Blocks.CAMPFIRE ||
                    nw == Blocks.CAMPFIRE || ne == Blocks.CAMPFIRE ||
                    sw == Blocks.CAMPFIRE || se == Blocks.CAMPFIRE ||
                    nn == Blocks.CAMPFIRE || ss == Blocks.CAMPFIRE ||
                    ww == Blocks.CAMPFIRE || ee == Blocks.CAMPFIRE ||
                    nnw == Blocks.CAMPFIRE || nne == Blocks.CAMPFIRE ||
                    sse == Blocks.CAMPFIRE || ssw == Blocks.CAMPFIRE ||
                    c1 == Blocks.CAMPFIRE || c2 == Blocks.CAMPFIRE ||
                    c3 == Blocks.CAMPFIRE || c4 == Blocks.CAMPFIRE ||
                    d == Blocks.LAVA || n == Blocks.LAVA || s == Blocks.LAVA ||
                    w == Blocks.LAVA || e == Blocks.LAVA ||
                    nw == Blocks.LAVA || ne == Blocks.LAVA ||
                    sw == Blocks.LAVA || se == Blocks.LAVA ||
                    nn == Blocks.LAVA || ss == Blocks.LAVA ||
                    ww == Blocks.LAVA || ee == Blocks.LAVA ||
                    nnw == Blocks.LAVA || nne == Blocks.LAVA ||
                    sse == Blocks.LAVA || ssw == Blocks.LAVA ||
                    dw == Blocks.LAVA || de == Blocks.LAVA ||
                    dn == Blocks.LAVA || ds == Blocks.LAVA ||
                    dnw == Blocks.LAVA || dne == Blocks.LAVA ||
                    dsw == Blocks.LAVA || dse == Blocks.LAVA ||
                    c1 == Blocks.LAVA || c2 == Blocks.LAVA ||
                    c3 == Blocks.LAVA || c4 == Blocks.LAVA ||
                    d == Blocks.FIRE || n == Blocks.FIRE || s == Blocks.FIRE ||
                    w == Blocks.FIRE || e == Blocks.FIRE ||
                    nw == Blocks.FIRE || ne == Blocks.FIRE ||
                    sw == Blocks.FIRE || se == Blocks.FIRE ||
                    nn == Blocks.FIRE || ss == Blocks.FIRE ||
                    ww == Blocks.FIRE || ee == Blocks.FIRE ||
                    nnw == Blocks.FIRE || nne == Blocks.FIRE ||
                    sse == Blocks.FIRE || ssw == Blocks.FIRE ||
                    c1 == Blocks.FIRE || c2 == Blocks.FIRE ||
                    c3 == Blocks.FIRE || c4 == Blocks.FIRE)
            {
                if(ZConfigManager.getInstance().enableTemperatureSystem() && heatresist == null && world.isDaytime() && world.getBiome(pos).getTemperature() >= 1.0F) {
                    overheattimer++;
                }
            }

            if (ZConfigManager.getInstance().useCryostone())
            {
                if (d == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || n == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || s == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        w == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || e == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        nw == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || ne == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        sw == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || se == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        nn == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || ss == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        ww == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || ee == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        nnw == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || nne == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        sse == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || ssw == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        c1 == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || c2 == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        c3 == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || c4 == SpecialBlocksInit.ANCIENT_CRYOSTONE.get()) {
                    if(world.getBiome(pos).getTemperature() >= 1.0F && world.isDaytime() && ZConfigManager.getInstance().enableTemperatureSystem()) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), ZConfigManager.getInstance().campfireDuration(), 0));
                    }
                }
        }
            if (!ZConfigManager.getInstance().useCryostone())
            {
                if (d == Blocks.SOUL_CAMPFIRE || n == Blocks.SOUL_CAMPFIRE || s == Blocks.SOUL_CAMPFIRE ||
                        w == Blocks.SOUL_CAMPFIRE || e == Blocks.SOUL_CAMPFIRE ||
                        nw == Blocks.SOUL_CAMPFIRE || ne == Blocks.SOUL_CAMPFIRE ||
                        sw == Blocks.SOUL_CAMPFIRE || se == Blocks.SOUL_CAMPFIRE ||
                        nn == Blocks.SOUL_CAMPFIRE || ss == Blocks.SOUL_CAMPFIRE ||
                        ww == Blocks.SOUL_CAMPFIRE || ee == Blocks.SOUL_CAMPFIRE ||
                        nnw == Blocks.SOUL_CAMPFIRE || nne == Blocks.SOUL_CAMPFIRE ||
                        sse == Blocks.SOUL_CAMPFIRE || ssw == Blocks.SOUL_CAMPFIRE ||
                        c1 == Blocks.SOUL_CAMPFIRE || c2 == Blocks.SOUL_CAMPFIRE ||
                        c3 == Blocks.SOUL_CAMPFIRE || c4 == Blocks.SOUL_CAMPFIRE)
                {
                    if(world.getBiome(pos).getTemperature() >= 1.0F && world.isDaytime() && ZConfigManager.getInstance().enableTemperatureSystem()) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), ZConfigManager.getInstance().campfireDuration(), 0));
                    }
                }
            }


        }
    }

    @SubscribeEvent
    public void armorcheck(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;
        ItemStack helmstack = player.getItemStackFromSlot(EquipmentSlotType.HEAD);
        ItemStack cheststack = player.getItemStackFromSlot(EquipmentSlotType.CHEST);
        ItemStack legstack = player.getItemStackFromSlot(EquipmentSlotType.LEGS);
        ItemStack feetstack = player.getItemStackFromSlot(EquipmentSlotType.FEET);
        if (ZConfigManager.getInstance().enableTemperatureSystem()) {
            if (ZConfigManager.getInstance().armorSetCount() == 4) {
                if (player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.DESERT_VOE_HEADBAND.get() &&
                        player.hasItemInSlot(EquipmentSlotType.CHEST) &&
                        cheststack.getItem() == ItemInit.DESERT_VOE_SPAULDER.get() &&
                        player.hasItemInSlot(EquipmentSlotType.LEGS) &&
                        legstack.getItem() == ItemInit.DESERT_VOE_TROUSERS.get() &&
                        player.hasItemInSlot(EquipmentSlotType.FEET) &&
                        feetstack.getItem() == ItemInit.DESERT_VOE_BOOTS.get()) {
                    if(ZConfigManager.getInstance().enableTemperatureSystem()) {
                        overheattimer = 0;
                    }
                    System.out.println("Immunity to heat!");
                }
            }
            if (ZConfigManager.getInstance().armorSetCount() == 3) {
                if (player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.DESERT_VOE_HEADBAND.get() &&
                        player.hasItemInSlot(EquipmentSlotType.CHEST) &&
                        cheststack.getItem() == ItemInit.DESERT_VOE_SPAULDER.get() &&
                        player.hasItemInSlot(EquipmentSlotType.LEGS) &&
                        legstack.getItem() == ItemInit.DESERT_VOE_TROUSERS.get()) {
                    if(ZConfigManager.getInstance().enableTemperatureSystem()) {
                        overheattimer = 0;
                    }
                    System.out.println("Immunity to heat!");
                }
            }
        }
    }

    @SubscribeEvent
    public void leafcheck(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;
        World world = player.world;
        BlockPos pos = new BlockPos(player.getPosition());
        ResourceLocation loc = world.func_241828_r().getRegistry(Registry.BIOME_KEY).getKey(world.getBiome(pos));
        Biome biome = ForgeRegistries.BIOMES.getValue(loc);
            if (world.getBiome(pos).getTemperature() >= 1.0F && player.isWet()) {
                if (ZConfigManager.getInstance().enableTemperatureSystem()) {
                    overheattimer--;
                    System.out.println("Temp dropping!");
                }
            }

            if (world.getBiome(pos).getTemperature() >= 1.0F &&
                    world.getBiome(pos).getTemperature() < 2.0F && world.isNightTime()) {
                if (ZConfigManager.getInstance().enableTemperatureSystem()) {
                    overheattimer--;
                    System.out.println("Temp dropping!");
                }
            }

            BlockPos posAbove = player.getPosition().up().up();
            BlockState blockStateAbove = player.world.getBlockState(posAbove);
            Material above = blockStateAbove.getMaterial(); //1 block above player-head
            BlockPos posAbove1 = player.getPosition().up().up().up();
            BlockState blockStateAbove1 = player.world.getBlockState(posAbove1);
            Material above1 = blockStateAbove1.getMaterial(); //2 blocks above player-head
            BlockPos posAbove2 = player.getPosition().up().up().up().up();
            BlockState blockStateAbove2 = player.world.getBlockState(posAbove2);
            Material above2 = blockStateAbove2.getMaterial(); //3 blocks above player-head
            BlockPos posAbove3 = player.getPosition().up().up().up().up().up();
            BlockState blockStateAbove3 = player.world.getBlockState(posAbove3);
            Material above3 = blockStateAbove3.getMaterial(); //4 blocks above player-head
            BlockPos posAbove4 = player.getPosition().up().up().up().up().up().up();
            BlockState blockStateAbove4 = player.world.getBlockState(posAbove4);
            Material above4 = blockStateAbove4.getMaterial(); //5 blocks above player-head
            if (above == Material.LEAVES || above1 == Material.LEAVES || above2 == Material.LEAVES ||
                    above3 == Material.LEAVES || above4 == Material.LEAVES) {
                if (world.getBiome(pos).getTemperature() >= 1.0F && world.isDaytime() && ZConfigManager.getInstance().enableTemperatureSystem() == true) {
                    overheattimer--;
                    System.out.println("Temp dropping!");
                }
            }
        }

    @SubscribeEvent
    public void tieronehotbiomecheck(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;
        World world = player.world;
        ItemStack helmstack = player.getItemStackFromSlot(EquipmentSlotType.HEAD);
        Effect heatresisteffect = EffectInit.HEAT_RESIST_EFFECT.get();
        EffectInstance heatresist = player.getActivePotionEffect(heatresisteffect);
        BlockPos pos = new BlockPos(player.getPosition());

        //Check for circlet bonuses
        if (ZConfigManager.getInstance().enableTemperatureSystem() &&
                world.getBiome(pos).getTemperature() >= 1F && world.getBiome(pos).getTemperature() < 2.0F) {
            player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h ->
            {
                if(heatresist != null)
                {
                    overheattimer = 0;
                }

                if(heatresist == null && world.canSeeSky(pos))
                {
                    overheattimer++;
                    System.out.println("Overheating!");
                }

                if (player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.SAPPHIRE_CIRCLET.get()) {
                    if (ZConfigManager.getInstance().enableTemperatureSystem() == true) {
                        overheattimer = 0;
                        System.out.println("Overheating!");
                    }
                }

                if (player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.UPGRADED_SAPPHIRE_CIRCLET.get()) {
                    if (ZConfigManager.getInstance().enableTemperatureSystem() == true) {
                        overheattimer = 0;
                        System.out.println("Overheating!");
                    }
                }

            });
        }

        //Check if the timer is more than the specified config-time then hurt the player
        if (overheattimer >= ZConfigManager.getInstance().defaultTempTimerThreshold()*2) {
            if (world.getBiome(pos).getTemperature() >= 1F && world.canSeeSky(pos)
                    && world.isDaytime() && world.getBiome(pos).getTemperature() < 2F && ZConfigManager.getInstance().enableTemperatureSystem()) {
                if(ZConfigManager.getInstance().allowTierOneBiomes()) {
                    player.attackEntityFrom(DamageSource.GENERIC, ZConfigManager.getInstance().tempDamage());
                    overheattimer = 0;
                }
            }
        }
    }

    @SubscribeEvent
    public void tiertwohotbiomecheck(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;
        World world = player.world;
        ItemStack helmstack = player.getItemStackFromSlot(EquipmentSlotType.HEAD);
        Effect heatresisteffect = EffectInit.HEAT_RESIST_EFFECT.get();
        EffectInstance heatresist = player.getActivePotionEffect(heatresisteffect);
        BlockPos pos = new BlockPos(player.getPosition());

        //Check deserts/badlands at day time and if the sky is visible with a condition of 'if heat-resist null'
        if (world.getBiome(pos).getTemperature() == 2.0F && world.isDaytime() && world.canSeeSky(pos) && heatresist == null
                && ZConfigManager.getInstance().enableTemperatureSystem() == true)
        {
            overheattimer++;
            System.out.println("Overheating!");
        }

        //Check for circlet bonuses
        if (world.getBiome(pos).getTemperature() == 2.0F)
        {
            player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h ->
            {
                if (h.getHeatResistCooldown() == 0 && player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.SAPPHIRE_CIRCLET.get())
                {
                    if(ZConfigManager.getInstance().enableTemperatureSystem() == true)
                    {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), circletmodifier, 0));
                        h.setHeatResistCooldown(circletmodifier);
                    }
                }

                if (h.getHeatResistCooldown() == 0 && player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.UPGRADED_SAPPHIRE_CIRCLET.get())
                {
                    if(ZConfigManager.getInstance().enableTemperatureSystem() == true)
                    {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), circletmodifier*2, 0));
                        h.setHeatResistCooldown(circletmodifier*2);
                    }
                }

            });
        }

        //Check if the timer is more than the specified config-time then hurt the player
        if (overheattimer >= ZConfigManager.getInstance().defaultTempTimerThreshold())
        {
            if(world.canSeeSky(pos)
                && world.isDaytime() && world.getBiome(pos).getTemperature() == 2F && ZConfigManager.getInstance().enableTemperatureSystem())
            {
                    player.attackEntityFrom(DamageSource.GENERIC, ZConfigManager.getInstance().tempDamage());
                    overheattimer = 0;
            }
        }

        }
}











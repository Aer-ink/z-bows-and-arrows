package com.ladestitute.zarrowsandstuff.subscriber;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.IPlayerDataCapability;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityFactory;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityHandler;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityStorage;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.brewing.BrewingRecipeRegistry;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class CommonEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticCommonSetup(FMLCommonSetupEvent event) {

        CapabilityManager.INSTANCE.register(IPlayerDataCapability.class, new PlayerDataCapabilityStorage(), PlayerDataCapabilityFactory::new);

        MinecraftForge.EVENT_BUS.register(new PlayerDataCapabilityHandler());

        BrewingRecipeRegistry.addRecipe(Ingredient.fromStacks(PotionUtils.addPotionToItemStack(new ItemStack(Items.POTION),
                Potions.AWKWARD)), Ingredient.fromItems(ItemInit.SPICY_PEPPER.get()), PotionUtils.addPotionToItemStack(new ItemStack(Items.POTION),
                EffectInit.COLD_RESIST_POTION.get()));
        BrewingRecipeRegistry.addRecipe(Ingredient.fromStacks(PotionUtils.addPotionToItemStack(new ItemStack(Items.POTION),
                EffectInit.COLD_RESIST_POTION.get())), Ingredient.fromItems(Items.GLOWSTONE_DUST), PotionUtils.addPotionToItemStack(new ItemStack(Items.POTION),
                EffectInit.COLD_RESIST_POTION.get()));
        BrewingRecipeRegistry.addRecipe(Ingredient.fromStacks(PotionUtils.addPotionToItemStack(new ItemStack(Items.POTION),
                Potions.AWKWARD)), Ingredient.fromItems(ItemInit.HYDROMELON_FRUIT.get()), PotionUtils.addPotionToItemStack(new ItemStack(Items.POTION),
                EffectInit.HEAT_RESIST_POTION.get()));
        BrewingRecipeRegistry.addRecipe(Ingredient.fromStacks(PotionUtils.addPotionToItemStack(new ItemStack(Items.POTION),
                EffectInit.HEAT_RESIST_POTION.get())), Ingredient.fromItems(Items.GLOWSTONE_DUST), PotionUtils.addPotionToItemStack(new ItemStack(Items.POTION),
                EffectInit.HEAT_RESIST_POTION.get()));

    }

    }

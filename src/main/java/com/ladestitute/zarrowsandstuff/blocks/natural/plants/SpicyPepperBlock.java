package com.ladestitute.zarrowsandstuff.blocks.natural.plants;

import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import net.minecraft.block.*;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.util.List;
import java.util.Random;

public class SpicyPepperBlock extends FallingBlock {

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public SpicyPepperBlock(AbstractBlock.Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(FACING, Direction.NORTH));
        // this.setDefaultState(defaultBlockState);

    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(FACING);
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(FACING)));
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getPlacementHorizontalFacing().getOpposite());
    }



    @Override
    public boolean removedByPlayer(BlockState state, World world, BlockPos pos, PlayerEntity player, boolean willHarvest, FluidState fluid) {
        Random rand1 = new Random();
        int harvest = rand1.nextInt(101);
        int bonusfruit = rand1.nextInt(11);
        ItemStack fruitstack = new ItemStack(ItemInit.SPICY_PEPPER.get());
        ItemEntity fruit = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), fruitstack);
        ItemStack fruitstack1 = new ItemStack(ItemInit.SPICY_PEPPER.get());
        ItemEntity fruit1 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), fruitstack1);
        ItemStack fruitstack2 = new ItemStack(ItemInit.SPICY_PEPPER.get());
        ItemEntity fruit2 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), fruitstack2);
        ItemStack seedstack = new ItemStack(ItemInit.SPICY_PEPPER_SEED.get());
        ItemEntity seed = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), seedstack);
        ItemStack seedstack1 = new ItemStack(ItemInit.SPICY_PEPPER_SEED.get());
        ItemEntity seed1 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), seedstack1);
        ItemStack seedstack2 = new ItemStack(ItemInit.SPICY_PEPPER_SEED.get());
        ItemEntity seed2 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), seedstack2);

        if(harvest < 18)
        {
            world.addEntity(fruit);
            world.addEntity(seed);
            world.addEntity(seed1);
            world.addEntity(seed2);
            if(bonusfruit > 6)
            {
                world.addEntity(fruit1);
            }
            if(bonusfruit < 2)
            {
                world.addEntity(fruit1);
                world.addEntity(fruit2);
            }
        }
        if(harvest > 18 && harvest < 41)
        {
            world.addEntity(fruit);
            world.addEntity(seed);
            world.addEntity(seed1);
            if(bonusfruit > 6)
            {
                world.addEntity(fruit1);
            }
            if(bonusfruit < 2)
            {
                world.addEntity(fruit1);
                world.addEntity(fruit2);
            }
        }
        if(harvest > 41)
        {
            world.addEntity(fruit);
            world.addEntity(seed);
            if(bonusfruit > 6)
            {
                world.addEntity(fruit1);
            }
            if(bonusfruit < 2)
            {
                world.addEntity(fruit1);
                world.addEntity(fruit2);
            }
        }
        world.setBlockState(pos, BlockInit.EMPTY_SPICY_PEPPER.get().getDefaultState());

        world.setBlockState(pos, Blocks.AIR.getDefaultState());
        return false;
    }

    @Override
    public void addInformation (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This pepper is exploding with spice. Cook with it to create dishes that will raise your body temperature and help you withstand the cold."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}


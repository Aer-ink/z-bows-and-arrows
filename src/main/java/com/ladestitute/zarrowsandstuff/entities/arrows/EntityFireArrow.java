package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityFireArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    public EntityFireArrow(EntityType<? extends EntityFireArrow> type, World world) {
        super(type, world);
    }

    public EntityFireArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.FIRE_ARROW.get(), shooter, worldIn);
        if(ZConfigManager.getInstance().experimentalFeatures()) {
            PlayerEntity player = Minecraft.getInstance().player;
            ItemStack handstack = player.getItemStackFromSlot(EquipmentSlotType.MAINHAND);
            if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) && handstack.getItem() == ItemInit.GOLDEN_BOW.get()) {
                if (ZConfigManager.getInstance().damageScaling() == 0) {
                    if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                        this.setDamage(this.getDamage() + 10F * 2);
                    } else this.setDamage(this.getDamage() * 2);
                }
                if (ZConfigManager.getInstance().damageScaling() == 1) {
                    if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                        this.setDamage(this.getDamage() + ((10F) / 2) * 2);
                    } else this.setDamage(this.getDamage() / 2 * 2);
                }
                if (ZConfigManager.getInstance().damageScaling() == 2) {
                    if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                        this.setDamage(this.getDamage() + ((10F) / 4) * 2);
                    } else this.setDamage(this.getDamage() / 4 * 2);
                }
            }

            if (ZConfigManager.getInstance().damageScaling() == 0) {
                if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                    this.setDamage(this.getDamage() + 10F);
                } else this.setDamage(this.getDamage());
            }
            if (ZConfigManager.getInstance().damageScaling() == 1) {
                if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                    this.setDamage(this.getDamage() + ((10F) / 2));
                } else this.setDamage(this.getDamage() / 2);
            }
            if (ZConfigManager.getInstance().damageScaling() == 2) {
                if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                    this.setDamage(this.getDamage() + ((10F) / 4));
                } else this.setDamage(this.getDamage() / 4);
            }
        }
    }

    public EntityFireArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.FIRE_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getArrowStack() {
        return new ItemStack(ItemInit.FIRE_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(this.isInWater())
        {
            BlockPos currentPos = this.getPosition();
            this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.FIREARROWWATER.get(), SoundCategory.PLAYERS, 2f, 1f);
            this.remove();
        }

        if(!this.isAirBorne || !this.isWet() || !this.isInWater())
        {
            BlockPos currentPos = this.getPosition();
            this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.FIREARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (this.inGround) {
            if (!this.isInWater() || !this.isWet()) {
                BlockPos currentPos = this.getPosition();
                this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.FIREARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);

                this.remove();
            }
        }

        if (!this.inGround || !this.isWet() || !this.isInWater()) {
            this.world.addParticle(ParticleTypes.FLAME, this.getPosX(), this.getPosY(), this.getPosZ(), 0.0D, 0.0D,
                    0.0D);
            this.world.addParticle(ParticleTypes.FLAME, this.getPosX(), this.getPosY(), this.getPosZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }


    @Override
    protected void onInsideBlock(BlockState state) {
        BlockState block = world.getBlockState(getPosition());
        if (this.inGround) {
            if(block == Blocks.SNOW.getDefaultState()||block == Blocks.COBWEB.getDefaultState()||
                    block == Blocks.BLACK_WALL_BANNER.getDefaultState()||
                    block == Blocks.BLUE_WALL_BANNER.getDefaultState()||
                    block == Blocks.BROWN_WALL_BANNER.getDefaultState()||block == Blocks.BLACK_BANNER.getDefaultState()||
                    block == Blocks.BLUE_BANNER.getDefaultState()||block == Blocks.BROWN_BANNER.getDefaultState()||
                    block == Blocks.CYAN_BANNER.getDefaultState()||block == Blocks.GRAY_BANNER.getDefaultState()||
                    block == Blocks.GREEN_BANNER.getDefaultState()||block == Blocks.LIGHT_BLUE_BANNER.getDefaultState()||
                    block == Blocks.LIGHT_GRAY_BANNER.getDefaultState()||block == Blocks.PURPLE_BANNER.getDefaultState()||
                    block == Blocks.PINK_BANNER.getDefaultState()||block == Blocks.ORANGE_BANNER.getDefaultState()||
                    block == Blocks.LIME_BANNER.getDefaultState()||block == Blocks.MAGENTA_BANNER.getDefaultState()||
                    block == Blocks.YELLOW_BANNER.getDefaultState()||block == Blocks.RED_BANNER.getDefaultState()||
                    block == Blocks.WHITE_BANNER.getDefaultState()||block == Blocks.BLACK_CARPET.getDefaultState()||
                    block == Blocks.BLUE_CARPET.getDefaultState()||block == Blocks.BROWN_CARPET.getDefaultState()||
                    block == Blocks.CYAN_CARPET.getDefaultState()||block == Blocks.GRAY_CARPET.getDefaultState()||
                    block == Blocks.GREEN_CARPET.getDefaultState()||block == Blocks.LIGHT_BLUE_CARPET.getDefaultState()||
                    block == Blocks.LIGHT_GRAY_CARPET.getDefaultState()||block == Blocks.PURPLE_CARPET.getDefaultState()||
                    block == Blocks.PINK_CARPET.getDefaultState()||block == Blocks.ORANGE_CARPET.getDefaultState()||
                    block == Blocks.LIME_CARPET.getDefaultState()||block == Blocks.MAGENTA_CARPET.getDefaultState()||
                    block == Blocks.YELLOW_CARPET.getDefaultState()||block == Blocks.RED_CARPET.getDefaultState()||
                    block == Blocks.WHITE_CARPET.getDefaultState()||
                    block == Blocks.CYAN_WALL_BANNER.getDefaultState()||block == Blocks.GRAY_WALL_BANNER.getDefaultState()||
                    block == Blocks.GREEN_WALL_BANNER.getDefaultState()||block == Blocks.LIGHT_BLUE_WALL_BANNER.getDefaultState()||
                    block == Blocks.LIGHT_GRAY_WALL_BANNER.getDefaultState()||block == Blocks.PURPLE_WALL_BANNER.getDefaultState()||
                    block == Blocks.PINK_WALL_BANNER.getDefaultState()||block == Blocks.ORANGE_WALL_BANNER.getDefaultState()||
                    block == Blocks.LIME_WALL_BANNER.getDefaultState()||block == Blocks.MAGENTA_WALL_BANNER.getDefaultState()||
                    block == Blocks.YELLOW_WALL_BANNER.getDefaultState()||block == Blocks.RED_WALL_BANNER.getDefaultState()||
                    block == Blocks.WHITE_WALL_BANNER.getDefaultState()||
                    block == Blocks.DEAD_BUSH.getDefaultState()||block == Blocks.FERN.getDefaultState()||
                    block == Blocks.GRASS.getDefaultState()||block == Blocks.VINE.getDefaultState()||
                    block == Blocks.RED_MUSHROOM.getDefaultState()||block == Blocks.BROWN_MUSHROOM.getDefaultState()||
                    block == Blocks.TRIPWIRE.getDefaultState()||block == Blocks.PINK_TULIP.getDefaultState()||
                    block == Blocks.ACACIA_SIGN.getDefaultState()||block == Blocks.BIRCH_SIGN.getDefaultState()||
                    block == Blocks.DARK_OAK_SIGN.getDefaultState()||block == Blocks.JUNGLE_SIGN.getDefaultState()||
                    block == Blocks.OAK_SIGN.getDefaultState()||block == Blocks.SPRUCE_SIGN.getDefaultState()||
                    block == Blocks.ACACIA_WALL_SIGN.getDefaultState()||block == Blocks.BIRCH_WALL_SIGN.getDefaultState()||
                    block == Blocks.DARK_OAK_WALL_SIGN.getDefaultState()||block == Blocks.JUNGLE_WALL_SIGN.getDefaultState()||
                    block == Blocks.OAK_WALL_SIGN.getDefaultState()||block == Blocks.SPRUCE_WALL_SIGN.getDefaultState()||
                    block == Blocks.DANDELION.getDefaultState()||block == Blocks.POPPY.getDefaultState()||
                    block == Blocks.PEONY.getDefaultState()||block == Blocks.ROSE_BUSH.getDefaultState()||
                    block == Blocks.LILAC.getDefaultState()||block == Blocks.SUNFLOWER.getDefaultState()||
                    block == Blocks.LILY_OF_THE_VALLEY.getDefaultState()||block == Blocks.CORNFLOWER.getDefaultState()||
                    block == Blocks.OXEYE_DAISY.getDefaultState()||block == Blocks.BLUE_ORCHID.getDefaultState()||
                    block == Blocks.ALLIUM.getDefaultState()||block == Blocks.AZURE_BLUET.getDefaultState()||
                    block == Blocks.ORANGE_TULIP.getDefaultState()||block == Blocks.RED_TULIP.getDefaultState()||
                    block == Blocks.WHITE_TULIP.getDefaultState())
            {
                BlockPos currentPos = this.getPosition();
                this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.FIREARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                world.setBlockState(this.getOnPosition().up(), Blocks.FIRE.getDefaultState());
                this.remove();
            }
        }
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected void arrowHit(LivingEntity living) {
        BlockPos currentPos = this.getPosition();
        living.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 40, 10));
        this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                SoundInit.FIREARROWHIT.get(), SoundCategory.PLAYERS, 1f, 1f);
        if(living.equals(EntityType.BLAZE)||living.equals(EntityType.MAGMA_CUBE)||living.equals(EntityType.HUSK))
        {
            this.setDamage(this.getDamage()/2);
        }
        if(living.equals(EntityType.POLAR_BEAR)||living.equals(EntityType.STRAY)||living.equals(EntityType.SNOW_GOLEM))
        {
            this.setDamage(this.getDamage()*2);
        }
        super.arrowHit(living);
    }
}

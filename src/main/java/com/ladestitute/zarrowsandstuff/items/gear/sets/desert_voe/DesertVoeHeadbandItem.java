package com.ladestitute.zarrowsandstuff.items.gear.sets.desert_voe;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class DesertVoeHeadbandItem extends ArmorItem {
    public DesertVoeHeadbandItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Gerudo-made armor for males sold rarely by Gerudo retailers. It contains sapphire, a gem that harnesses the power of ice to make the heat more bearable."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}
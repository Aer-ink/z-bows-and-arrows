package com.ladestitute.zarrowsandstuff.world.gen;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.registries.ConfiguredFeaturesInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID)
public class ZGeneration {
    @SubscribeEvent(priority =  EventPriority.HIGH)
    public static void modifyBiomes(BiomeLoadingEvent event) {

        RegistryKey<Biome> biomeRegistryKey = RegistryKey.getOrCreateKey(Registry.BIOME_KEY, event.getName());

            if (event.getClimate().temperature == 0.5F || event.getCategory() == Biome.Category.EXTREME_HILLS){
                event.getGeneration().withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, ConfiguredFeaturesInit.PILE_ANCIENT_WRECKAGE);
            }
        if (event.getCategory() == Biome.Category.FOREST && event.getName().toString() != "minecraft:dark_forest" ||
                event.getCategory() == Biome.Category.FOREST && event.getName().toString() != "minecraft:dark_forest_hills"){
            if(ZConfigManager.getInstance().generatemodplants())
            {
                event.getGeneration().withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, ConfiguredFeaturesInit.PATCH_STAMELLA_MUSHROOMS);
            }
        }
        if (event.getCategory() == Biome.Category.FOREST && event.getName().toString() == "minecraft:dark_forest" ||
                event.getCategory() == Biome.Category.FOREST && event.getName().toString() == "minecraft:dark_forest_hills"){
            if(ZConfigManager.getInstance().generatemodplants())
            {
                event.getGeneration().withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, ConfiguredFeaturesInit.PATCH_STAMELLA_MUSHROOMSDARK);
            }
        }
        if (event.getCategory() == Biome.Category.FOREST){
            event.getGeneration().withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, ConfiguredFeaturesInit.PILE_ANCIENT_WRECKAGE_FOREST);
        }
            if (event.getCategory() == Biome.Category.DESERT || event.getCategory() == Biome.Category.MESA || event.getCategory() == Biome.Category.SAVANNA){
               event.getGeneration().withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, ConfiguredFeaturesInit.PILE_ANCIENT_WRECKAGE_DESERT);
            }
        if (event.getCategory() == Biome.Category.DESERT){
            if(ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, ConfiguredFeaturesInit.PATCH_WILD_HYDROMELONS);
            }
        }
        if (event.getCategory() == Biome.Category.EXTREME_HILLS || event.getCategory() == Biome.Category.PLAINS || event.getCategory() == Biome.Category.TAIGA)
        {
            if(ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, ConfiguredFeaturesInit.PATCH_WILD_PEPPERS);
            }
        }
        if (event.getCategory() == Biome.Category.ICY||event.getCategory() == Biome.Category.TAIGA){
            event.getGeneration().withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, ConfiguredFeaturesInit.PILE_ANCIENT_WRECKAGE_COLD);
        }
        if (event.getCategory() == Biome.Category.SWAMP)
        {
            event.getGeneration().withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, ConfiguredFeaturesInit.PILE_ANCIENT_WRECKAGE_SWAMP);
        }
        if (event.getCategory() == Biome.Category.JUNGLE)
        {
            if(ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, ConfiguredFeaturesInit.PATCH_WILD_JUNGLEHYDROMELONS);
            }
            event.getGeneration().withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, ConfiguredFeaturesInit.PILE_ANCIENT_WRECKAGE_JUNGLE);

        }
        if (event.getClimate().temperature < 0.3F && event.getClimate().temperature > 0.16F){
            event.getGeneration().withFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, ConfiguredFeaturesInit.ORE_DEPOSIT);
        }
    }
}


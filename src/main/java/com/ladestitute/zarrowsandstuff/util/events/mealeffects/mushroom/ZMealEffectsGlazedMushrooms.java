package com.ladestitute.zarrowsandstuff.util.events.mealeffects.mushroom;

import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsGlazedMushrooms {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS1.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(800);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS2.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(400);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS3.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1000);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS4.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(800);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS5.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 18);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1200);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS6.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(400);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS7.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 18);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1000);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS8.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1200);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS9.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1000);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS10.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 20);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1600);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS11.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(800);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS12.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 20);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1400);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS13.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1800);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS14.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(400);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS15.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 20);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1000);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS16.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1600);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS17.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 18);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1600);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS18.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 16);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1400);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS19.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1800);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS20.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1400);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS21.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1600);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS22.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(2200);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS23.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 16);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(800);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS24.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1800);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS25.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1800);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS26.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(2200);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS27.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 16);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(400);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS28.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1000);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS29.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(1600);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS30.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.addStamina(2200);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
        }
    }
}



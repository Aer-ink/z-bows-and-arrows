package com.ladestitute.zarrowsandstuff.items.arrows;

import com.ladestitute.zarrowsandstuff.entities.arrows.EntityAncientArrow;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityFireArrow;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

import net.minecraft.item.Item.Properties;

public class AncientArrowItem extends ArrowItem {
    public AncientArrowItem(Properties properties)
    {
        super(properties.maxStackSize(64));
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        EntityAncientArrow arrow = new EntityAncientArrow(worldIn, shooter);
        arrow.pickupStatus = AbstractArrowEntity.PickupStatus.ALLOWED;
        return arrow;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("An arrow created using ancient technology. To be struck with one is to be consigned to oblivion in an instant. It deals devastating damage, even against Guardians."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}


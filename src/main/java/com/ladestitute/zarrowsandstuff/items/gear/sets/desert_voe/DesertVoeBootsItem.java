package com.ladestitute.zarrowsandstuff.items.gear.sets.desert_voe;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class DesertVoeBootsItem extends ArmorItem {
    public DesertVoeBootsItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Boots made for men by the Gerudo. They protect the wearer's feet from both sand and scorching heat."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}
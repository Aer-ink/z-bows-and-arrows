package com.ladestitute.zarrowsandstuff.items.gear;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

import java.util.List;

public class TopazEarringsItem extends ArmorItem {
    public TopazEarringsItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Earrings made by Gerudo craft workers. They're made with topaz, a gem that harnesses the power of lightning to increase electricity resistance."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}

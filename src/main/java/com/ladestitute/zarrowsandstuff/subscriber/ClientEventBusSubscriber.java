package com.ladestitute.zarrowsandstuff.subscriber;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.client.render.arrows.*;
import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import com.ladestitute.zarrowsandstuff.util.KeyboardUtil;
import com.ladestitute.zarrowsandstuff.util.events.ZStatsGUIHandler;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticClientSetup(FMLClientSetupEvent event) {
        event.setPhase(EventPriority.HIGH);
        MinecraftForge.EVENT_BUS.register(new ZStatsGUIHandler());
        RenderTypeLookup.setRenderLayer(BlockInit.ORE_DEPOSIT.get(), RenderType.getTranslucent());
        RenderTypeLookup.setRenderLayer(BlockInit.SPICY_PEPPER.get(), RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(BlockInit.STAMELLA_SHROOM.get(), RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.HATTACHED_STEM.get(), RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.HYDROMELON_STEM.get(), RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(BlockInit.EMPTY_SPICY_PEPPER.get(), RenderType.getCutout());
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.FIRE_ARROW.get(), FireArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.ICE_ARROW.get(), IceArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.SHOCK_ARROW.get(), ShockArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.BOMB_ARROW.get(), BombArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.ANCIENT_ARROW.get(), AncientArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.LIGHT_ARROW.get(), LightArrowRender::new);
    }
}

package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.List;

public class EntityShockArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    float fullshockwavedamage = 40F;

    public EntityShockArrow(EntityType<? extends EntityShockArrow> type, World world) {
        super(type, world);
    }

    public EntityShockArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.SHOCK_ARROW.get(), shooter, worldIn);
        if(ZConfigManager.getInstance().damageScaling() == 0) {
            this.setDamage(this.getDamage() + 20F);
        }
        if(ZConfigManager.getInstance().damageScaling() == 1) {
            this.setDamage(this.getDamage() + 20F/2);
        }
        if(ZConfigManager.getInstance().damageScaling() == 2) {
            this.setDamage(this.getDamage() + 20F/4);
        }
    }

    public EntityShockArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.SHOCK_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getArrowStack() {
        return new ItemStack(ItemInit.SHOCK_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(this.isInWater())
        {
            BlockPos currentPos = this.getPosition();
            this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.SHOCKARROWWATER.get(), SoundCategory.PLAYERS, 2f, 1f);
            this.remove();
        }

        if(!this.isAirBorne || !this.isWet() || !this.isInWater())
        {
            BlockPos currentPos = this.getPosition();
            this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.SHOCKARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (this.inGround) {
            if (!this.isInWater() || !this.isWet()) {
                BlockPos currentPos = this.getPosition();
                this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.SHOCKARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);

                this.remove();
            }
        }

        if (!this.inGround || !this.isWet() || !this.isInWater()) {
            this.world.addParticle(ParticleTypes.ENCHANTED_HIT, this.getPosX(), this.getPosY(), this.getPosZ(), 0.0D, 0.0D,
                    0.0D);
            this.world.addParticle(ParticleTypes.ENCHANTED_HIT, this.getPosX(), this.getPosY(), this.getPosZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    private List<LivingEntity> getPlayersAround() {
        float range = 5.5F;
        return world.getEntitiesWithinAABB(LivingEntity.class, new AxisAlignedBB(this.getPosX() + 0.5 - range, this.getPosY() + 0.5 - range, this.getPosX() + 0.5 - range, this.getPosX() + 0.5 - range, this.getPosY() + 0.5 - range, this.getPosZ() + 0.5 - range));
    }

    @Override
    protected void arrowHit(LivingEntity living) {
        BlockPos currentPos = this.getPosition();
        this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                SoundInit.SHOCKARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
        living.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 40, 10));
        if (living.isWet() && !living.isInWater() || living.equals(EntityType.IRON_GOLEM)) {
            this.setDamage(this.getDamage() * 2);
        }
        ItemStack mainstack = living.getItemStackFromSlot(EquipmentSlotType.MAINHAND);
        ItemStack offstack = living.getItemStackFromSlot(EquipmentSlotType.OFFHAND);
        if (living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SWORD ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_SWORD ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_AXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_AXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SHOVEL ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_HOE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_HOE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.CROSSBOW ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.SHIELD) {
            living.entityDropItem(mainstack);
            living.setItemStackToSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY);
        }
        if (living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_SWORD ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_SWORD ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_AXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_AXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_SHOVEL ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_HOE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_HOE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.CROSSBOW ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.SHIELD) {
            living.entityDropItem(offstack);
            living.setItemStackToSlot(EquipmentSlotType.OFFHAND, ItemStack.EMPTY);
        }
        if (living.isWet() && living.isInWater()) {
            double radius = 10;
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(this.getOnPosition()).grow(radius).expand(0.0D, this.world.getHeight(), 0.0D);
            List<LivingEntity> list = this.world.getEntitiesWithinAABB(LivingEntity.class, axisalignedbb);
                for (LivingEntity livingEntity : list) {
                    if (ZConfigManager.getInstance().selfShockArrowImmunity() == false)
                    {
                        ItemStack helmstack = living.getItemStackFromSlot(EquipmentSlotType.HEAD);
                        if (livingEntity instanceof PlayerEntity && living.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.TOPAZ_EARRINGS.get()) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                            }
                        }
                        if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get()) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/16);
                            }
                        } else if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                        else if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                    }
                    if (ZConfigManager.getInstance().selfShockArrowImmunity() == true)
                    {
                        if (!(livingEntity instanceof PlayerEntity)) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                        }
                    }
                }
            super.arrowHit(living);
        }
    }

    @Override
    protected void doBlockCollisions() {
        BlockState block = world.getBlockState(getPosition());
        double radius = 30;
        AxisAlignedBB axisalignedbb = new AxisAlignedBB(this.getOnPosition()).grow(radius).expand(0.0D, this.world.getHeight(), 0.0D);
        List<LivingEntity> list = this.world.getEntitiesWithinAABB(LivingEntity.class, axisalignedbb);
        if (block == Blocks.ACTIVATOR_RAIL.getDefaultState() || block == Blocks.ANCIENT_DEBRIS.getDefaultState() ||
                block == Blocks.ANVIL.getDefaultState() || block == Blocks.BELL.getDefaultState() ||
                block == Blocks.CAULDRON.getDefaultState() || block == Blocks.CHAIN.getDefaultState() ||
                block == Blocks.CHIPPED_ANVIL.getDefaultState() || block == Blocks.DAMAGED_ANVIL.getDefaultState() ||
                block == Blocks.DETECTOR_RAIL.getDefaultState() || block == Blocks.IRON_BLOCK.getDefaultState() ||
                block == Blocks.GOLD_BLOCK.getDefaultState() || block == Blocks.HOPPER.getDefaultState() ||
                block == Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE.getDefaultState() || block == Blocks.IRON_BARS.getDefaultState() ||
                block == Blocks.IRON_DOOR.getDefaultState() || block == Blocks.IRON_TRAPDOOR.getDefaultState() ||
                block == Blocks.LANTERN.getDefaultState() || block == Blocks.LIGHT_WEIGHTED_PRESSURE_PLATE.getDefaultState() ||
                block == Blocks.LODESTONE.getDefaultState() || block == Blocks.MOVING_PISTON.getDefaultState() ||
                block == Blocks.PISTON.getDefaultState() || block == Blocks.PISTON_HEAD.getDefaultState()) {
            for (LivingEntity livingEntity : list) {
                if (ZConfigManager.getInstance().selfShockArrowImmunity() == false)
                {
                    ItemStack helmstack = livingEntity.getItemStackFromSlot(EquipmentSlotType.HEAD);
                    if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.TOPAZ_EARRINGS.get()) {
                        if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                        }
                    }

                    if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get()) {
                        if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/16);
                        }
                    } else if(ZConfigManager.getInstance().damageScaling() == 0)
                    {
                        livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                    }
                    if(ZConfigManager.getInstance().damageScaling() == 1) {
                        livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                    }
                    if(ZConfigManager.getInstance().damageScaling() == 2) {
                        livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                    }

                    else if(ZConfigManager.getInstance().damageScaling() == 0)
                    {
                        livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                    }
                    if(ZConfigManager.getInstance().damageScaling() == 1) {
                        livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                    }
                    if(ZConfigManager.getInstance().damageScaling() == 2) {
                        livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                    }
                }
                if (ZConfigManager.getInstance().selfShockArrowImmunity() == true)
                {
                    if (!(livingEntity instanceof PlayerEntity)) {
                        if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                    }
                }
                    this.remove();

            }
        }
        super.doBlockCollisions();
    }

    @Override
        protected void onInsideBlock (BlockState state){
            BlockState block = world.getBlockState(getPosition());
            double radius = 10;
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(this.getOnPosition()).grow(radius).expand(0.0D, this.world.getHeight(), 0.0D);
            List<LivingEntity> list = this.world.getEntitiesWithinAABB(LivingEntity.class, axisalignedbb);
            if (block == Blocks.WATER.getDefaultState()) {
                BlockPos currentPos = this.getPosition();
                this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.SHOCKARROWWATER.get(), SoundCategory.PLAYERS, 2.5f, 1f);
                for (LivingEntity livingEntity : list) {
                    if (ZConfigManager.getInstance().selfShockArrowImmunity() == false)
                    {
                        ItemStack helmstack = livingEntity.getItemStackFromSlot(EquipmentSlotType.HEAD);
                        if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.TOPAZ_EARRINGS.get()) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                            }

                            if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get()) {
                                if(ZConfigManager.getInstance().damageScaling() == 0)
                                {
                                    livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                                }
                                if(ZConfigManager.getInstance().damageScaling() == 1) {
                                    livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                                }
                                if(ZConfigManager.getInstance().damageScaling() == 2) {
                                    livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/16);
                                }
                            } else if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                        } else if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }

                        if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get()) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/16);
                            }
                        } else if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                    }
                    if (!ZConfigManager.getInstance().selfShockArrowImmunity() == true)
                    {
                        if (!(livingEntity instanceof PlayerEntity)) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.attackEntityFrom(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                        }
                    }
                        this.remove();
                }
                super.onInsideBlock(state);
            }
        }

    }






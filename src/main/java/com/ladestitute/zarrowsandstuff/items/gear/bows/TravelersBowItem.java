package com.ladestitute.zarrowsandstuff.items.gear.bows;

import com.ladestitute.zarrowsandstuff.util.ZToolMaterials;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class TravelersBowItem extends BowItem {

    public TravelersBowItem(ZToolMaterials toolmaterial, Properties builder) {
        super(builder.maxStackSize(1).maxDamage(422));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A small bow used by travelers for protection. It doesn't do a lot of damage, but it can be used to attack foes from a distance."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}

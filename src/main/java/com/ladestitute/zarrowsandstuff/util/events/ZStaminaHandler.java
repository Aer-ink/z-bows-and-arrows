package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.util.KeyboardUtil;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

public class ZStaminaHandler {

    @SubscribeEvent
    public void basics(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;

        World world = player.world;

        player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
            if (h.getStamina() < 0) {
                h.setStamina(0);
            }

            if (h.getStamina() > h.getMaxStamina()) {
                h.setStamina(h.getMaxStamina());
            }

            if(h.getStamina() > 0 && player.collidedHorizontally && KeyboardUtil.isHoldingR())
            {
                BlockPos posSouth = player.getPosition().south();
                BlockState blockStateSouth = player.world.getBlockState(posSouth);
                Material south = blockStateSouth.getMaterial();
                BlockPos posNorth = player.getPosition().north();
                BlockState blockStateNorth = player.world.getBlockState(posNorth);
                Material north = blockStateSouth.getMaterial();
                BlockPos posWest = player.getPosition().west();
                BlockState blockStateWest = player.world.getBlockState(posWest);
                Material west = blockStateSouth.getMaterial();
                BlockPos posEast = player.getPosition().east();
                BlockState blockStateEast = player.world.getBlockState(posEast);
                Material east = blockStateSouth.getMaterial();
                if(south != Material.LEAVES || north != Material.LEAVES ||
                        west != Material.LEAVES || east != Material.LEAVES ||
                        south != Material.WEB || north != Material.WEB ||
                        west != Material.WEB || east != Material.WEB) {
                    player.setMotion(0, 0.15, 0);
                    h.subtractStamina(1);
                    if(world.isRaining())
                    {
                        Random rand1 = new Random();
                        int slipchance = rand1.nextInt(11);

                        if(slipchance == 0)
                        {
                            h.subtractStamina(2);
                        }
                    }
                }
            }

            if(player.isOnGround() && h.getStamina() < h.getMaxStamina())
            {
                if(h.getStamina() >= 0 && h.getStamina() < 25) {
                    h.addStamina(12);
                }
                if(h.getStamina() < 50 && h.getStamina() > 25) {
                    h.addStamina(9);
                }
                if(h.getStamina() > 50 && h.getStamina() < 75) {
                    h.addStamina(6);
                }
                if(h.getStamina() > 75) {
                    h.addStamina(3);
                }
            }
        });
    }
}

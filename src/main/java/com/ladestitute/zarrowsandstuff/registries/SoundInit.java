package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class SoundInit {
        public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, ZArrowsMain.MOD_ID);

        //Arrows
        public static final RegistryObject<SoundEvent> FIREARROWFLYING = SOUNDS.register("entity.projectiles.firearrowflying", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.firearrowflying")));
        public static final RegistryObject<SoundEvent> FIREARROWHIT = SOUNDS.register("entity.projectiles.firearrowhit", ()
            -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.firearrowhit")));
        public static final RegistryObject<SoundEvent> FIREARROWWATER = SOUNDS.register("entity.projectiles.firearrowwater", ()
            -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.firearrowwater")));

        public static final RegistryObject<SoundEvent> ICEARROWFLYING = SOUNDS.register("entity.projectiles.icearrowflying", ()
            -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.icearrowflying")));
        public static final RegistryObject<SoundEvent> ICEARROWHIT = SOUNDS.register("entity.projectiles.icearrowhit", ()
            -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.icearrowhit")));
        public static final RegistryObject<SoundEvent> ICEARROWWATER = SOUNDS.register("entity.projectiles.icearrowwater", ()
            -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.icearrowwater")));

        public static final RegistryObject<SoundEvent> SHOCKARROWFLYING = SOUNDS.register("entity.projectiles.shockarrowflying", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.shockarrowflying")));
        public static final RegistryObject<SoundEvent> SHOCKARROWHIT = SOUNDS.register("entity.projectiles.shockarrowhit", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.shockarrowhit")));
        public static final RegistryObject<SoundEvent> SHOCKARROWWATER = SOUNDS.register("entity.projectiles.shockarrowwater", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.shockarrowwater")));

        public static final RegistryObject<SoundEvent> BOMBARROWFLYING = SOUNDS.register("entity.projectiles.bombarrowflying", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.bombarrowflying")));

        public static final RegistryObject<SoundEvent> ANCIENTARROWFLYING = SOUNDS.register("entity.projectiles.ancientarrowflying", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.ancientarrowflying")));
        public static final RegistryObject<SoundEvent> ANCIENTARROWHIT = SOUNDS.register("entity.projectiles.ancientarrowhit", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.ancientarrowhit")));

        public static final RegistryObject<SoundEvent> LIGHTARROWFLYING = SOUNDS.register("entity.projectiles.lightarrowflying", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.lightarrowflying")));
        public static final RegistryObject<SoundEvent> LIGHTARROWHIT = SOUNDS.register("entity.projectiles.lightarrowhit", ()
                -> new SoundEvent(new ResourceLocation(ZArrowsMain.MOD_ID, "entity.projectiles.lightarrowhit")));

    }

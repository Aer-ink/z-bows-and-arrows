package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import net.minecraft.block.*;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootContext;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;

public class ZBlocksHandler {

    @SubscribeEvent
    public void plantpeppers(PlayerInteractEvent.RightClickBlock event) {
        BlockPos plantpos = event.getPos();
        BlockState state = event.getWorld().getBlockState(plantpos);
        if (event.getWorld().isRemote) {
            return;
        }
        if (state.getBlock() instanceof GrassBlock || state.getBlock() instanceof FarmlandBlock) {
            if (event.getItemStack().getItem() == ItemInit.SPICY_PEPPER_SEED.get()) {
                BlockPos pepperplant = event.getPos().up();
                event.getWorld().setBlockState(pepperplant, BlockInit.EMPTY_SPICY_PEPPER.get().getDefaultState());

                ItemStack pepperseed = ItemInit.SPICY_PEPPER_SEED.get().getDefaultInstance();
                if (event.getPlayer().inventory.hasItemStack(pepperseed))
                    for (ItemStack seed : event.getPlayer().inventory.mainInventory)
                        if (seed.getItem() == ItemInit.SPICY_PEPPER_SEED.get()) {
                            seed.shrink(1);
                        }
            }
            }
        }

    @SubscribeEvent
    public void planthydromelons(PlayerInteractEvent.RightClickBlock event) {
        BlockPos plantpos = event.getPos();
        BlockState state = event.getWorld().getBlockState(plantpos);
        if (event.getWorld().isRemote) {
            return;
        }
        if (state.getBlock() instanceof GrassBlock || state.getBlock() instanceof SandBlock || state.getBlock() instanceof FarmlandBlock) {
            if (event.getItemStack().getItem() == ItemInit.HYDROMELON_SEED.get()) {
                BlockPos pepperplant = event.getPos().up();
                LivingEntity player = event.getPlayer();
                event.getWorld().setBlockState(pepperplant, SpecialBlocksInit.HYDROMELON_STEM.get().getDefaultState());

                ItemStack hydromelon = ItemInit.HYDROMELON_SEED.get().getDefaultInstance();
                if (event.getPlayer().inventory.hasItemStack(hydromelon))
                    for (ItemStack seed : event.getPlayer().inventory.mainInventory)
                        if (seed.getItem() == ItemInit.HYDROMELON_SEED.get()) {
                            seed.shrink(1);
                        }
            }
        }
    }
}

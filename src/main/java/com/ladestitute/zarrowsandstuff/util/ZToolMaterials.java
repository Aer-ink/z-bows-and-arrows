package com.ladestitute.zarrowsandstuff.util;

import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;

public enum ZToolMaterials implements IItemTier {

    TRAVELERS(1.2F, 0f, 422, 0, 0, Items.STICK),
    SOLDIERS(4.26F, 0f, 556, 0, 0, Items.IRON_INGOT),
    SILVER(4.38F, 0f, 576, 0, 0, Items.IRON_INGOT),
    GOLDEN(4.26F, 0f, 637, 0, 0, Items.GOLD_INGOT);
    private float attackDmg, efficiency;
    private int durability, harvestLevel, enchantabillity;
    private Item repairMaterial;
    private ZToolMaterials(float attackDmg, float efficiency, int durability, int harvestLevel, int enchantability, Item repairMaterial) {
        this.attackDmg = attackDmg;
        this.efficiency = efficiency;
        this.durability = durability; //All Tools same
        this.harvestLevel = harvestLevel;
        this.enchantabillity = enchantability; // Wood 15, Stone 5, Iron 14, Gold 22, Dia 10, 1.16Netherite 15
        this.repairMaterial = repairMaterial;
    }

    @Override
    public int getMaxUses() {
        return this.durability;
    }

    @Override
    public float getEfficiency() {
        return this.efficiency;
    }

    @Override
    public float getAttackDamage() {
        return this.attackDmg;
    }

    @Override
    public int getHarvestLevel() {
        return this.harvestLevel;
    }

    @Override
    public int getEnchantability() {
        return this.enchantabillity;
    }

    @Override
    public Ingredient getRepairMaterial() {
        return Ingredient.fromItems(this.repairMaterial);
    }

}

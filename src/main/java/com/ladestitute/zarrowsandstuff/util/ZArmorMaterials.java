package com.ladestitute.zarrowsandstuff.util;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;

public enum ZArmorMaterials implements IArmorMaterial
{
    SNOWQUILL("snowquill", 363, new int[] {3, 3, 3, 3}, 0, Items.LEATHER,
           SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0.0f, 0.0f),
    DESERT_VOE("desert_voe", 363, new int[] {3, 3, 3, 3}, 0, Items.RABBIT_HIDE,
            SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0.0f, 0.0f),
    AMBER_EARRINGS("amber_e", 363, new int[] {4, 4, 4, 4}, 0, ItemInit.AMBER.get(),
            SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0f, 0.0f),
    OPAL_EARRINGS("opal_e", 363, new int[] {3, 3, 3, 3}, 0, ItemInit.OPAL.get(),
    SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0f, 0.0f),
    TOPAZ_EARRINGS("topaz_e", 363, new int[] {3, 3, 3, 3}, 0, ItemInit.TOPAZ.get(),
            SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0f, 0.0f),
    RUBY_CIRCLET("ruby_c", 363, new int[] {3, 3, 3, 3}, 0, ItemInit.RUBY.get(),
            SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0f, 0.0f),
    SAPPHIRE_CIRCLET("sapphire_c", 363, new int[] {3, 3, 3, 3}, 0, ItemInit.SAPPHIRE.get(),
            SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0f, 0.0f),
    UPGRADED_TOPAZ_EARRINGS("topaz_eu", 508, new int[] {5, 5, 5, 5}, 0, ItemInit.TOPAZ.get(),
            SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0f, 0.0f),
    UPGRADED_RUBY_CIRCLET("ruby_cu", 508, new int[] {5, 5, 5, 5}, 0, ItemInit.RUBY.get(),
            SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0f, 0.0f),
    UPGRADED_SAPPHIRE_CIRCLET("sapphire_cu", 508, new int[] {5, 5, 5, 5}, 0, ItemInit.SAPPHIRE.get(),
            SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0f, 0.0f);

    private String name;
    private final SoundEvent soundEvent;
    private int durability, enchantability;
    private int[] dmgReductionAmounts;
    private float toughness, knockbackResistance;
    private Item repairItem;
    private static final int[] max_damage_array = new int[] {13, 15, 16, 11};

    private ZArmorMaterials(String name, int durability, int[]dmgReductionAmounts, int enchantability, Item repairItem, SoundEvent soundEvent, float toughness, float knockbackResistance)
    {
        this.name = name;
        this.soundEvent = soundEvent;
        this.durability = durability; //helmet*11, chest*16, leg*15, boots*13   //Dia 33, Iron 15, Gold 7, Leather 5
        this.enchantability = enchantability; //Leather 15, Chain 12, Iron 9, Gold 25, Dia 10, 1.16Netherite 15
        this.dmgReductionAmounts = dmgReductionAmounts; //{Boots, Leg, Chest, Helmet}
        this.toughness = toughness; //Dia 2, 1.16Netherite 3
        this.repairItem = repairItem;
        this.knockbackResistance = knockbackResistance;

    }

    @Override
    public int getDurability(EquipmentSlotType slot)
    {
        return max_damage_array[slot.getIndex()] * this.durability;
    }

    @Override
    public int getDamageReductionAmount(EquipmentSlotType slot)
    {
        return this.dmgReductionAmounts[slot.getIndex()];
    }

    @Override
    public int getEnchantability()
    {
        return this.enchantability;
    }

    @Override
    public SoundEvent getSoundEvent() {
        return this.soundEvent;
    }

    @Override
    public Ingredient getRepairMaterial()
    {
        return Ingredient.fromItems(this.repairItem);
    }

    @Override
    public String getName()
    {
        return ZArrowsMain.MOD_ID + ":" + this.name;
    }

    @Override
    public float getToughness()
    {
        return this.toughness;
    }

    @Override
    public float getKnockbackResistance()
    {
        return this.knockbackResistance;
    }
}
package com.ladestitute.zarrowsandstuff.blocks.ancient;

import net.minecraft.block.FallingBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;

import java.util.List;

import net.minecraft.block.AbstractBlock.Properties;

public class AncientWreckageJungleBlock extends FallingBlock {

    public AncientWreckageJungleBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Ancient wreckage of a long dead guardian, it has decayed beyond recognition"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}

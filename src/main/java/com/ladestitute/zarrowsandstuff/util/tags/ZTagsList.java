package com.ladestitute.zarrowsandstuff.util.tags;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import net.minecraft.block.Blocks;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.ItemTagsProvider;
import net.minecraft.item.Items;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ZTagsList {
    public static class ItemTagsDataGen extends ItemTagsProvider {

        public ItemTagsDataGen(DataGenerator generatorIn, BlockTagsProvider blockTagsProvider,
                               ExistingFileHelper existingFileHelper) {
            super(generatorIn, blockTagsProvider, ZArrowsMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void registerTags() {
            //Adding your own blocks/items from init must have the type-subtype builder before it
            // To refresh tags after edits below, run preparerundata then run rundata
            //copy the tags folder to your mod's data folder, ie resources/data/modid/tags

            getOrCreateBuilder(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_AMBER);
            getOrCreateBuilder(ZTags.Items.GEMS_AMBER).add(ItemInit.AMBER.get());

            getOrCreateBuilder(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_OPAL);
            getOrCreateBuilder(ZTags.Items.GEMS_OPAL).add(ItemInit.OPAL.get());
            getOrCreateBuilder(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_RUBY);
            getOrCreateBuilder(ZTags.Items.GEMS_RUBY).add(ItemInit.RUBY.get());
            getOrCreateBuilder(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_SAPPHIRE);
            getOrCreateBuilder(ZTags.Items.GEMS_SAPPHIRE).add(ItemInit.SAPPHIRE.get());
            getOrCreateBuilder(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_TOPAZ);
            getOrCreateBuilder(ZTags.Items.GEMS_TOPAZ).add(ItemInit.TOPAZ.get());

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_MEAT).add(Items.BEEF);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_MEAT).add(Items.MUTTON);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_MEAT).add(Items.RABBIT);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_MEAT).add(Items.PORKCHOP);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_ALLMEAT).add(Items.BEEF);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_ALLMEAT).add(Items.MUTTON);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_ALLMEAT).add(Items.RABBIT);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_ALLMEAT).add(Items.PORKCHOP);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            getOrCreateBuilder(ZTags.Items.FOOD_ALLMEAT).add(Items.CHICKEN);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_BIRD);
            getOrCreateBuilder(ZTags.Items.FOOD_BIRD).add(Items.CHICKEN);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SPICY1);
            getOrCreateBuilder(ZTags.Items.FOOD_SPICY1).add(ItemInit.SPICY_PEPPER.get());

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_CHILLY1);
            getOrCreateBuilder(ZTags.Items.FOOD_CHILLY1).add(ItemInit.HYDROMELON_FRUIT.get());

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ENERGIZING1);
            getOrCreateBuilder(ZTags.Items.FOOD_ENERGIZING1).add(ItemInit.STAMELLA_SHROOM.get());

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT).add(Items.MELON_SLICE);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT).add(Items.CHORUS_FRUIT);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT2);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT2).add(Items.APPLE);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT2);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT2).add(Items.SWEET_BERRIES);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT4);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT4).add(ItemInit.HYDROMELON_FRUIT.get());
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT4);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT4).add(ItemInit.SPICY_PEPPER.get());

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT5).add(Items.MELON_SLICE);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT5).add(Items.CHORUS_FRUIT);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT5).add(Items.SWEET_BERRIES);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT5).add(ItemInit.HYDROMELON_FRUIT.get());
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            getOrCreateBuilder(ZTags.Items.FOOD_FRUIT5).add(ItemInit.SPICY_PEPPER.get());

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_WILDBERRY);
            getOrCreateBuilder(ZTags.Items.FOOD_WILDBERRY).add(Items.SWEET_BERRIES);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MUSHROOM);
            getOrCreateBuilder(ZTags.Items.FOOD_MUSHROOM).add(Items.RED_MUSHROOM);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MUSHROOM);
            getOrCreateBuilder(ZTags.Items.FOOD_MUSHROOM).add(Items.BROWN_MUSHROOM);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MUSHROOM);
            getOrCreateBuilder(ZTags.Items.FOOD_MUSHROOM).add(Items.WARPED_FUNGUS);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MUSHROOM);
            getOrCreateBuilder(ZTags.Items.FOOD_MUSHROOM).add(Items.CRIMSON_FUNGUS);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_VEGETABLE);
            getOrCreateBuilder(ZTags.Items.FOOD_VEGETABLE).add(Items.BEETROOT);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_VEGETABLE);
            getOrCreateBuilder(ZTags.Items.FOOD_VEGETABLE).add(Items.POTATO);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_VEGETABLE);
            getOrCreateBuilder(ZTags.Items.FOOD_VEGETABLE).add(Items.CARROT);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SEAFOOD);
            getOrCreateBuilder(ZTags.Items.FOOD_SEAFOOD).add(Items.PUFFERFISH);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SEAFOOD);
            getOrCreateBuilder(ZTags.Items.FOOD_SEAFOOD).add(Items.TROPICAL_FISH);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SEAFOOD);
            getOrCreateBuilder(ZTags.Items.FOOD_SEAFOOD).add(Items.COD);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SEAFOOD);
            getOrCreateBuilder(ZTags.Items.FOOD_SEAFOOD).add(Items.SALMON);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_HONEY);
            getOrCreateBuilder(ZTags.Items.FOOD_HONEY).add(Items.HONEYCOMB);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_HONEY);
            getOrCreateBuilder(ZTags.Items.FOOD_HONEY).add(Items.HONEY_BOTTLE);

            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SUGAR);
            getOrCreateBuilder(ZTags.Items.FOOD_SUGAR).add(Items.SUGAR);
            getOrCreateBuilder(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SUGAR);
            getOrCreateBuilder(ZTags.Items.FOOD_SUGAR).add(Items.SUGAR_CANE);

            //getOrCreateBuilder(ZTags.Items.MATERIAL).addTag(ZTags.Items.MATERIAL_WOOL);
           // getOrCreateBuilder(ZTags.Items.MATERIAL_WOOL).add(Blocks.BLACK_WOOL.asItem());
        }
    }

    public static class BlockTagsDataGen extends BlockTagsProvider {

        public BlockTagsDataGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
            super(generatorIn, ZArrowsMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void registerTags() {
            //    getOrCreateBuilder(SpelunkTags.Blocks.ORES_RUBY).add(BlockInit.ORERUBY.get());
        }
    }
}


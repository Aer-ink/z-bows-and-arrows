package com.ladestitute.zarrowsandstuff.util.config.subscreens;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigHomeScreen;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.list.OptionsRowList;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.BooleanOption;
import net.minecraft.client.settings.SliderPercentageOption;
import net.minecraft.util.Util;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import javax.annotation.Nonnull;
import java.util.Objects;

public final class ZConfigTemperatureScreen extends Screen {
    // URL to the help page with more information about this mod's settings
    private static final String MORE_INFO_URL = "https://gitlab.com/LaDestitute/z-bows-and-arrows/-/wikis/Temperature-System";

    // Distance between this GUI's title and the top of the screen
    private static final int TITLE_HEIGHT = 8;

    // Distance between the options list's top and the top of the screen
    private static final int OPTIONS_LIST_TOP_HEIGHT = 24;

    // Distance between the options list's bottom and the bottom of the screen
    private static final int OPTIONS_LIST_BOTTOM_OFFSET = 32;

    // Distance between the top of each button below the options list and the screen bottom
    private static final int BOTTOM_BUTTON_HEIGHT_OFFSET = 26;

    // Height of each item in the options list
    private static final int OPTIONS_LIST_ITEM_HEIGHT = 25;

    // Width of each button below the options list
    private static final int BOTTOM_BUTTON_WIDTH = 150;

    // The object for registering options on this screen and how they function
    private OptionsRowList optionsRowList;

    //Our ConfigManager instance
    private static final ZConfigManager CMI = ZConfigManager.getInstance();

    // The parent screen of this screen
    public Screen parentScreen = new ZConfigHomeScreen(this);

    //Construct a new screen instance
    public ZConfigTemperatureScreen(Screen parentScreen) {
        super(new TranslationTextComponent("zarrowsandstuff.configGui.temperatureoptions.title",
                ZArrowsMain.NAME));
        //   this.temperatureScreen = temperatureScreen;
    }

    // Initializes this GUI with options list and buttons.
    @Override
    protected void init() {
        this.optionsRowList = new OptionsRowList(
                Objects.requireNonNull(this.minecraft), this.width, this.height,
                OPTIONS_LIST_TOP_HEIGHT,
                this.height - OPTIONS_LIST_BOTTOM_OFFSET,
                OPTIONS_LIST_ITEM_HEIGHT);
        //this.optionsRowList.addOption(new Button(this.width, this.height,
        //        new TranslationTextComponent("zarrowsandstuff.configGui.enabletemperaturesystem.title"), (p_213059_1_) -> {
        //   this.minecraft.displayGuiScreen(new VideoSettingsScreen(this, this.
        //  ));
        this.optionsRowList.addOption(new BooleanOption(
                "zarrowsandstuff.configGui.enabletemperaturesystem.title",
                unused -> CMI.enableTemperatureSystem(),
                (unused, newValue) -> CMI.enableTemperatureSystem(newValue)
        ));
        this.optionsRowList.addOption(new BooleanOption(
                "zarrowsandstuff.configGui.alwaysshowtemp.title",
                unused -> CMI.alwaysshowTemp(),
                (unused, newValue) -> CMI.changeshowTemp(newValue)
        ));
        this.optionsRowList.addOption(new BooleanOption(
                "zarrowsandstuff.configGui.allowtieronebiomes.title",
                unused -> CMI.allowTierOneBiomes(),
                (unused, newValue) -> CMI.changeAllowTierOneBiomes(newValue)
        ));
        this.optionsRowList.addOption(new BooleanOption(
                "zarrowsandstuff.configGui.usecryostone.title",
                unused -> CMI.useCryostone(),
                (unused, newValue) -> CMI.changeUsecryostone(newValue)
        ));
        this.optionsRowList.addOption(new SliderPercentageOption(
                "zarrowsandstuff.configGui.campfireduration.title",
                600, 3000, 1.0F,
                unused -> (double) CMI.campfireDuration(),
                (unused, newValue) -> CMI.changeCampfireDuration(newValue.intValue()),
                (gs, option) -> new StringTextComponent(I18n.format(
                        "zarrowsandstuff.configGui.campfireduration.title"
                ) + ": " + (int) option.get(gs))));
        this.optionsRowList.addOption(new SliderPercentageOption(
                "zarrowsandstuff.configGui.tempdamage.title",
                1, 10, 1.0F,
                unused -> (double) CMI.tempDamage(),
                (unused, newValue) -> CMI.setTempDamage(newValue.intValue()),
                (gs, option) -> new StringTextComponent(I18n.format(
                        "zarrowsandstuff.configGui.tempdamage.title"
                ) + ": " + (int) option.get(gs))));
        this.optionsRowList.addOption(new SliderPercentageOption(
                "zarrowsandstuff.configGui.mountainylevel.title",
                87, 123, 1.0F,
                unused -> (double) CMI.mountainYLevel(),
                (unused, newValue) -> CMI.changeMountainYLevel(newValue.intValue()),
                (gs, option) -> new StringTextComponent(I18n.format(
                        "zarrowsandstuff.configGui.mountainylevel.title"
                ) + ": " + (int) option.get(gs))));
        this.optionsRowList.addOption(new SliderPercentageOption(
                "zarrowsandstuff.configGui.defaulttemptimerthreshold.title",
                100, 800, 1.0F,
                unused -> (double) CMI.defaultTempTimerThreshold(),
                (unused, newValue) -> CMI.changeDefaultTempTimerThreshold(newValue.intValue()),
                (gs, option) -> new StringTextComponent(I18n.format(
                        "zarrowsandstuff.configGui.defaulttemptimerthreshold.title"
                ) + ": " + (int) option.get(gs))));
        this.children.add(this.optionsRowList);

        this.addButton(new Button(
                (this.width - 4) / 2 - BOTTOM_BUTTON_WIDTH,
                this.height - BOTTOM_BUTTON_HEIGHT_OFFSET,
                BOTTOM_BUTTON_WIDTH, 20,
                new TranslationTextComponent("zarrowsandstuff.configGui.moreinfo.title"),
                button -> Util.getOSType().openURI(MORE_INFO_URL))
        );
        this.addButton(new Button(
                (this.width + 4) / 2,
                this.height - BOTTOM_BUTTON_HEIGHT_OFFSET,
                BOTTOM_BUTTON_WIDTH, 20,
                new TranslationTextComponent("gui.back"),
                button -> Objects.requireNonNull(this.minecraft)
                        .displayGuiScreen(parentScreen))
        );
    }

    // Draw the GUI
    // @param matrixStack the matrix stack
    // @param mouseX horizontal location of the mouse
    // @param mouseY vertical location of the mouse
    // @param partialTicks number of partial ticks
    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void render(@Nonnull MatrixStack matrixStack,
                       int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        this.optionsRowList.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.font, this.title.getString(),
                this.width / 2, TITLE_HEIGHT, 0xFFFFFF);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
    }

    // Since Minecraft 1.16, the {@link Screen#onClose()} method no
    // longer closes the current screen. Instead, the way to close the
    // current screen becomes
    @Override
    public void onClose() {
        CMI.save();
    }

}

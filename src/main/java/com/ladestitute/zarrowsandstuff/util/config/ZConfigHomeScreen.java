package com.ladestitute.zarrowsandstuff.util.config;

import com.ladestitute.zarrowsandstuff.util.config.subscreens.*;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.gui.screen.MainMenuScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.client.gui.screen.ModListScreen;

public class ZConfigHomeScreen extends Screen {

    /**
     * URL to the help page with more information about this mod's settings
     */
    private static final String MORE_INFO_URL =
            "https://gitlab.com/LaDestitute/z-bows-and-arrows/-/wikis/Config";

    private static final int BOTTOM_BUTTON_HEIGHT_OFFSET = 26;

    /**
     * Height of each item in the options list
     */
    private static final int OPTIONS_LIST_ITEM_HEIGHT = 25;

    /**
     * Width of each button below the options list
     */
    private static final int BOTTOM_BUTTON_WIDTH = 150;

    /**
     * The {@link ZConfigManager} instance
     */
    private static final ZConfigManager CMI = ZConfigManager.getInstance();

    public ZConfigHomeScreen(Screen parentScreen) {
        super(new TranslationTextComponent("zarrowsandstuff.configGui.title"));
    }

    public Screen rootScreen = new MainMenuScreen(false);

    protected void init() {

        this.addButton(new Button(this.width / 2 - 80, this.height / 6 + 2, 160, 20,
                new TranslationTextComponent("zarrowsandstuff.configGui.general.title"), button -> {
            this.minecraft.displayGuiScreen(new ZConfigGeneralScreen(this));
        }));
        this.addButton(new Button(this.width / 2 - 80, this.height / 6 + 2 + (24*1), 160, 20,
                new TranslationTextComponent("zarrowsandstuff.configGui.vanillalike.title"), button -> {
            this.minecraft.displayGuiScreen(new ZConfigVanillaLikeScreen(this));
        }));
        this.addButton(new Button(this.width / 2 - 80, this.height / 6 + 2 + (24*2), 160, 20,
                new TranslationTextComponent("zarrowsandstuff.configGui.combat.title"), button -> {
            this.minecraft.displayGuiScreen(new ZConfigCombatScreen(this));
        }));
        this.addButton(new Button(this.width / 2 - 80, this.height / 6 + 2 + (24*3), 160, 20,
                new TranslationTextComponent("zarrowsandstuff.configGui.worldgen.title"), button -> {
            this.minecraft.displayGuiScreen(new ZConfigGenerationScreen(this));
        }));
        this.addButton(new Button(this.width / 2 - 80, this.height / 6 + 2 + (24*4), 160, 20,
                new TranslationTextComponent("zarrowsandstuff.configGui.temperatureoptions.title"), button -> {
            this.minecraft.displayGuiScreen(new ZConfigTemperatureScreen(this));
        }));
        //Bottom buttons
        this.addButton(new Button(this.width - 4 / 2 - BOTTOM_BUTTON_WIDTH - 60,
                this.height - BOTTOM_BUTTON_HEIGHT_OFFSET,
                BOTTOM_BUTTON_WIDTH, 20,
                new TranslationTextComponent("zarrowsandstuff.configGui.back.title"), button -> {
            this.minecraft.displayGuiScreen(new ModListScreen(this.rootScreen));
        }));

    }

    @Override
    public void onClose() {
        CMI.save();
    }

    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        drawCenteredString(matrixStack, this.font, this.title, this.width / 2, 15, 16777215);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
    }


}
package com.ladestitute.zarrowsandstuff.blocks;

import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;

import java.util.List;

public class AncientCryostoneBlock extends Block {
    public AncientCryostoneBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void addInformation (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("Ancient sheikah tech. It seems to drawn in the heat around it, thermodynamically changing the air around it by unknown meaning."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}


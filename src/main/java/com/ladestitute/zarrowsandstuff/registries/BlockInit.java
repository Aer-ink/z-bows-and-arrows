package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.blocks.ancient.*;
import com.ladestitute.zarrowsandstuff.blocks.natural.OreDepositBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.SnowstoneBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.EmptySpicyPepperBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.SpicyPepperBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.StamellaShroomBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon.HydromelonBlock;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class BlockInit {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            ZArrowsMain.MOD_ID);

    //Stone
    public static final RegistryObject<Block> ANCIENT_WRECKAGE = BLOCKS.register("ancient_wreckage",
            () -> new AncientWreckageBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_DESERT = BLOCKS.register("ancient_wreckage_desert",
            () -> new AncientWreckageDesertBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_COLD = BLOCKS.register("ancient_wreckage_cold",
            () -> new AncientWreckageColdBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_FOREST = BLOCKS.register("ancient_wreckage_forest",
            () -> new AncientWreckageForestBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_SWAMP = BLOCKS.register("ancient_wreckage_swamp",
            () -> new AncientWreckageSwampBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_JUNGLE = BLOCKS.register("ancient_wreckage_jungle",
            () -> new AncientWreckageJungleBlock(PropertiesInit.WRECKAGE));

    public static final RegistryObject<Block> SNOWSTONE = BLOCKS.register("snowstone",
            () -> new SnowstoneBlock(PropertiesInit.SNOWSTONE));

    public static final RegistryObject<Block> HYDROMELON = BLOCKS.register("hydromelon",
            () -> new HydromelonBlock(PropertiesInit.HYDROMELON));

    //Ore
    public static final RegistryObject<Block> ORE_DEPOSIT = BLOCKS.register("ore_deposit", () -> new OreDepositBlock(PropertiesInit.ORE_DEPOSIT));

    //Plants
    public static final RegistryObject<Block> EMPTY_SPICY_PEPPER = BLOCKS.register("empty_spicy_pepper_plant",
            () -> new EmptySpicyPepperBlock(PropertiesInit.SPICY_PEPPER));
    public static final RegistryObject<Block> SPICY_PEPPER = BLOCKS.register("spicy_pepper_plant",
            () -> new SpicyPepperBlock(PropertiesInit.SPICY_PEPPER));
    public static final RegistryObject<Block> STAMELLA_SHROOM = BLOCKS.register("stamella_shroom_block",
            () -> new StamellaShroomBlock(AbstractBlock.Properties.from(Blocks.RED_MUSHROOM)));
}

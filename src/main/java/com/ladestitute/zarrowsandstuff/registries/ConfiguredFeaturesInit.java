package com.ladestitute.zarrowsandstuff.registries;

import com.google.common.collect.ImmutableSet;
import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import net.minecraft.block.Blocks;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;

public class ConfiguredFeaturesInit {
    //public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE = FeaturesInit.ANCIENT_WRECKAGE_FEATURE.get().withConfiguration(new BlockClusterFeatureConfig.Builder(new SimpleBlockStateProvider(BlockInit.ANCIENT_WRECKAGE.get().getDefaultState()), SimpleBlockPlacer.PLACER).xSpread(4).ySpread(5).zSpread(4).tries(60).build()).range(40).chance(256);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE = Feature.FOREST_ROCK.withConfiguration(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE.get().getDefaultState())).withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT
    ).func_242732_c(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_DESERT = Feature.FOREST_ROCK.withConfiguration(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_DESERT.get().getDefaultState())).withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT
    ).func_242732_c(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_COLD = Feature.FOREST_ROCK.withConfiguration(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_COLD.get().getDefaultState())).withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT
    ).func_242732_c(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_FOREST = Feature.FOREST_ROCK.withConfiguration(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_FOREST.get().getDefaultState())).withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT
    ).func_242732_c(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_SWAMP = Feature.FOREST_ROCK.withConfiguration(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_SWAMP.get().getDefaultState())).withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT
    ).func_242732_c(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_JUNGLE = Feature.FOREST_ROCK.withConfiguration(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_JUNGLE.get().getDefaultState())).withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT
    ).func_242732_c(1).range(40).chance(32);

    public static final ConfiguredFeature<?, ?> SURFACE_DEPOSITS = Feature.ORE
            .withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD, BlockInit.ORE_DEPOSIT.get().getDefaultState(), 3))
            .withPlacement(Placement.RANGE.configure(new TopSolidRangeConfig(63, 63, 89))).square();

    public static final ConfiguredFeature<?, ?> ORE_DEPOSIT =
            Feature.RANDOM_PATCH.withConfiguration(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.ORE_DEPOSIT.get().getDefaultState()),
                    SimpleBlockPlacer.PLACER).xSpread(1).ySpread(1).zSpread(1).tries(12).blacklist(ImmutableSet.of(Blocks.SNOW.getDefaultState(),Blocks.SNOW_BLOCK.getDefaultState())).whitelist(ImmutableSet.of(Blocks.STONE)).build())
                    .withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT).chance(16);

    public static final ConfiguredFeature<?, ?> PATCH_WILD_HYDROMELONS =
            Feature.RANDOM_PATCH.withConfiguration(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.HYDROMELON.get().getDefaultState()),
                    SimpleBlockPlacer.PLACER).xSpread(4).ySpread(1).zSpread(4).tries(6).whitelist(ImmutableSet.of(Blocks.SAND)).build())
                    .withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT).chance(6);

    public static final ConfiguredFeature<?, ?> PATCH_WILD_JUNGLEHYDROMELONS =
            Feature.RANDOM_PATCH.withConfiguration(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.HYDROMELON.get().getDefaultState()),
                    SimpleBlockPlacer.PLACER).xSpread(4).ySpread(1).zSpread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                    .withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT).chance(8);

    public static final ConfiguredFeature<?, ?> PATCH_WILD_PEPPERS =
    Feature.RANDOM_PATCH.withConfiguration(new BlockClusterFeatureConfig.Builder(new
    SimpleBlockStateProvider(BlockInit.SPICY_PEPPER.get().getDefaultState()),
     SimpleBlockPlacer.PLACER).xSpread(4).ySpread(1).zSpread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
     .withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT).chance(6);

    public static final ConfiguredFeature<?, ?> PATCH_STAMELLA_MUSHROOMS =
            Feature.RANDOM_PATCH.withConfiguration(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.STAMELLA_SHROOM.get().getDefaultState()),
                    SimpleBlockPlacer.PLACER).xSpread(4).ySpread(1).zSpread(4).tries(4).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                    .withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT).chance(4);

    public static final ConfiguredFeature<?, ?> PATCH_STAMELLA_MUSHROOMSDARK =
            Feature.RANDOM_PATCH.withConfiguration(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.STAMELLA_SHROOM.get().getDefaultState()),
                    SimpleBlockPlacer.PLACER).xSpread(4).ySpread(1).zSpread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                    .withPlacement(Features.Placements.HEIGHTMAP_PLACEMENT).chance(6);

    public static void registerConfiguredFeatures() {
        register("ancient_wreckage_pile", PILE_ANCIENT_WRECKAGE);
        register("ancient_wreckage_pile_desert", PILE_ANCIENT_WRECKAGE_DESERT);
        register("ancient_wreckage_pile_cold", PILE_ANCIENT_WRECKAGE_COLD);
        register("ancient_wreckage_pile_forest", PILE_ANCIENT_WRECKAGE_FOREST);
        register("ancient_wreckage_pile_swamp", PILE_ANCIENT_WRECKAGE_SWAMP);
        register("ancient_wreckage_pile_jungle", PILE_ANCIENT_WRECKAGE_JUNGLE);
        register("ore_deposit", ORE_DEPOSIT);
        register("patch_wild_hydromelons", PATCH_WILD_HYDROMELONS);
        register("patch_wild_junglehydromelons", PATCH_WILD_JUNGLEHYDROMELONS);
        register("patch_wild_peppers", PATCH_WILD_PEPPERS);
        register("patch_stamella_mushrooms", PATCH_STAMELLA_MUSHROOMS);
        register("patch_stamella_mushroomsdark", PATCH_STAMELLA_MUSHROOMSDARK);
    }

    private static void register(String identifier, ConfiguredFeature<?, ?> configuredFeature) {
        Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, ZArrowsMain.MOD_ID + ":" + identifier, configuredFeature);
    }
}

package com.ladestitute.zarrowsandstuff.util.zarrowscap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class PlayerDataCapabilityStorage implements Capability.IStorage<IPlayerDataCapability> {

    @Nullable
    @Override
    public INBT writeNBT(Capability<IPlayerDataCapability> capability, IPlayerDataCapability instance, Direction side) {
        CompoundNBT tag = new CompoundNBT();
        //It doesn't matter what you name the tags but its best to be consistent
        tag.putInt("coldresistcooldown", instance.getColdResistCooldown());
        tag.putInt("heatresistcooldown", instance.getHeatResistCooldown());
        tag.putInt("foodcooldown", instance.getFoodCooldown());
        tag.putInt("stamina", instance.getStamina());
        tag.putInt("maxstamina", instance.getMaxStamina());
        return tag;
    }

    @Override
    public void readNBT(Capability<IPlayerDataCapability> capability, IPlayerDataCapability instance, Direction side, INBT nbt) {
        CompoundNBT tag = (CompoundNBT) nbt;
        instance.setColdResistCooldown(tag.getInt("coldresistcooldown"));
        instance.setHeatResistCooldown(tag.getInt("heatresistcooldown"));
        instance.setFoodCooldown(tag.getInt("foodcooldown"));
        instance.setStamina(tag.getInt("stamina"));
        instance.setMaxStamina(tag.getInt("maxstamina"));
    }
}
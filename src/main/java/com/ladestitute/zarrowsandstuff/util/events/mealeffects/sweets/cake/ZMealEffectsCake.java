package com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets.cake;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsCake {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.FRUITCAKE.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.FRUITCAKE1.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 7000, 0));
                        h.setHeatResistCooldown(7000);
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.FRUITCAKE2.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 7000, 0));
                        h.setColdResistCooldown(7000);
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.FRUITCAKE3.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                    h.addStamina(500);
                });
            }
        }
    }
}

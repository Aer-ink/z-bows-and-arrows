package com.ladestitute.zarrowsandstuff.util.events.mealeffects;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.PotionUtils;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsHandlerCore {

    @SubscribeEvent
    public void basics(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;

        World world = player.world;

        player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {

            if (h.getFoodCooldown() < 0) {
                h.setFoodCooldown(0);
            }

            if (h.getFoodCooldown() > 0) {
                h.subtractFoodCooldown(1);
            }

        });
    }

    //Handling for vanilla potion effects, this way we have modify the behavior to make it more in line with the mod
    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            if (PotionUtils.getPotionFromItem(event.getItem()) == EffectInit.COLD_RESIST_POTION.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                    player.removeActivePotionEffect(oppositeresist);
                    h.setColdResistCooldown(9000);
                    System.out.println("Cooldown is " + h.getColdResistCooldown());
                });
            }
            if (PotionUtils.getPotionFromItem(event.getItem()) == EffectInit.HEAT_RESIST_POTION.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                    player.removeActivePotionEffect(oppositeresist);
                    h.setHeatResistCooldown(9000);
                    System.out.println("Cooldown is " + h.getColdResistCooldown());
                });
            }
        }
    }

    @SubscribeEvent
    public void cooldown(LivingEntityUseItemEvent.Start event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            if (PotionUtils.getPotionFromItem(event.getItem()) == EffectInit.COLD_RESIST_POTION.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if(h.getColdResistCooldown() >= 1)
                    {
                        if(ZConfigManager.getInstance().resistPotioncooldown()) {
                            event.setCanceled(true);
                        }
                    }
                });
            }
            if (PotionUtils.getPotionFromItem(event.getItem()) == EffectInit.HEAT_RESIST_POTION.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if(h.getHeatResistCooldown() >= 1)
                        if(ZConfigManager.getInstance().resistPotioncooldown()) {
                            event.setCanceled(true);
                        }
                });
            }
        }
    }
}

package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import org.spongepowered.asm.mixin.MixinEnvironment;

public class EntityBombArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    public EntityBombArrow(EntityType<? extends EntityBombArrow> type, World world) {
        super(type, world);
    }

    public EntityBombArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.BOMB_ARROW.get(), shooter, worldIn);
        if(ZConfigManager.getInstance().damageScaling() == 0) {
            this.setDamage(this.getDamage() + 50F);
        }
        if(ZConfigManager.getInstance().damageScaling() == 1) {
            this.setDamage(this.getDamage() + (50F)/2);
        }
        if(ZConfigManager.getInstance().damageScaling() == 2) {
            this.setDamage(this.getDamage() + (50F)/4);
        }
    }

    public EntityBombArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.BOMB_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getArrowStack() {
        return new ItemStack(ItemInit.BOMB_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(this.isInWater())
        {
            this.remove();
        }

        if(!this.isAirBorne || !this.isWet() || !this.isInWater())
        {
            BlockPos currentPos = this.getPosition();
            this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.BOMBARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (!this.inGround || !this.isWet() || !this.isInWater()) {
            this.world.addParticle(ParticleTypes.CAMPFIRE_COSY_SMOKE, this.getPosX(), this.getPosY(), this.getPosZ(), 0.0D, 0.0D,
                    0.0D);
            this.world.addParticle(ParticleTypes.CAMPFIRE_COSY_SMOKE, this.getPosX(), this.getPosY(), this.getPosZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }



    @Override
    protected void arrowHit(LivingEntity living) {
        if(!this.isWet()||!this.isInWater()) {
            this.createExplosion();
        }
    }

    @Override
    protected void onInsideBlock(BlockState state) {
        if (this.isAirBorne && !this.inWater||this.isAirBorne && !this.isWet()) {
            BlockState block = world.getBlockState(getPosition());
            if (block != Blocks.WATER.getDefaultState()||block != Blocks.AIR.getDefaultState()||block != Blocks.CAVE_AIR.getDefaultState()||block != Blocks.VOID_AIR.getDefaultState()) {
                this.createExplosion();
            }
        }
    }


    public void createExplosion() {
        if(!world.isRemote) {
            world.createExplosion(null, this.getPosX(), this.getPosY(), this.getPosZ(), 3F, true, Explosion.Mode.DESTROY);
            this.remove();
        }
    }

    }




package com.ladestitute.zarrowsandstuff.util.zarrowscap;

public interface IPlayerDataCapability {
    //Pretty simple, just an interface with an integer and some methods
    int getColdResistCooldown();
    int getHeatResistCooldown();
    int getFoodCooldown();
    int getStamina();
    int getMaxStamina();

    void setColdResistCooldown(int coldresistcooldown);

    void addColdResistCooldown(int coldresistcooldown);

    void subtractColdResistCooldown(int coldresistcooldown);

    void setHeatResistCooldown(int heatresistcooldown);

    void addHeatResistCooldown(int heatresistcooldown);

    void subtractHeatResistCooldown(int heatresistcooldown);

    void setFoodCooldown(int foodcooldown);

    void addFoodCooldown(int foodcooldown);

    void subtractFoodCooldown(int foodcooldown);

    void setStamina(int stamina);

    void addStamina(int stamina);

    void subtractStamina(int stamina);

    void setMaxStamina(int maxstamina);

    void addMaxStamina(int maxstamina);

    void subtractMaxStamina(int maxstamina);

}

package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.text.DecimalFormat;

public class ZStatsGUIHandler {

    @SubscribeEvent
    public void renderGameOverlayEvent(RenderGameOverlayEvent.Post event) {
        if (event.isCancelable() || event.getType() != RenderGameOverlayEvent.ElementType.EXPERIENCE) {
            return;
        }

        FontRenderer fontRenderer = Minecraft.getInstance().fontRenderer;

        MatrixStack matrixStack = new MatrixStack();

        BlockPos pos = new BlockPos(Minecraft.getInstance().player.getPosition());

        DecimalFormat df = new DecimalFormat("###.##");
        Minecraft.getInstance().player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
            if(ZConfigManager.getInstance().alwaysshowTemp()) {
                fontRenderer.drawStringWithShadow(matrixStack, "Temp: " + df.format(Minecraft.getInstance().player.world.getBiome(pos).getTemperature(pos)) + "F", 350, 10, 0xFFFFFF);
            }
            if(ZConfigManager.getInstance().alwaysshowStamina()) {
                fontRenderer.drawStringWithShadow(matrixStack, "Stamina: " + h.getStamina(), 350, 25, 0xFFFFFF);
            }
            else if(h.getStamina() < h.getMaxStamina())
            {
                fontRenderer.drawStringWithShadow(matrixStack, "Stamina: " + h.getStamina(), 350, 25, 0xFFFFFF);
            }
            });
    }
    }

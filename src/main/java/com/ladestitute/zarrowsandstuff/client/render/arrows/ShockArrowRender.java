package com.ladestitute.zarrowsandstuff.client.render.arrows;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityShockArrow;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ShockArrowRender extends ArrowRenderer<EntityShockArrow> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows/shock_arrow.png");

    public ShockArrowRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getEntityTexture(EntityShockArrow entity) {
        return TEXTURE;
    }


}



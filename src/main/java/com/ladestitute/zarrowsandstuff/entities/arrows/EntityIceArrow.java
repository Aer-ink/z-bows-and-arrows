package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityIceArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    public EntityIceArrow(EntityType<? extends EntityIceArrow> type, World world) {
        super(type, world);
    }

    public EntityIceArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.ICE_ARROW.get(), shooter, worldIn);
        if(ZConfigManager.getInstance().experimentalFeatures())
        {
            PlayerEntity player = Minecraft.getInstance().player;
            ItemStack handstack = player.getItemStackFromSlot(EquipmentSlotType.MAINHAND);
            if(player.hasItemInSlot(EquipmentSlotType.MAINHAND) && handstack.getItem() == ItemInit.SILVER_BOW.get())
            {
                if(ZConfigManager.getInstance().damageScaling() == 0) {
                    this.setDamage(this.getDamage() + 10F*2);
                }
                if(ZConfigManager.getInstance().damageScaling() == 1) {
                    this.setDamage(this.getDamage() + ((10F)/2)*2);
                }
                if(ZConfigManager.getInstance().damageScaling() == 2) {
                    this.setDamage(this.getDamage() + ((10F)/4)*2);
                }
            }
            else if(ZConfigManager.getInstance().damageScaling() == 0) {
                this.setDamage(this.getDamage() + 10F);
            }
            if(ZConfigManager.getInstance().damageScaling() == 1) {
                this.setDamage(this.getDamage() + 10F/2);
            }
            if(ZConfigManager.getInstance().damageScaling() == 2) {
                this.setDamage(this.getDamage() + 10F/4);
            }
        }
    }

    public EntityIceArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.ICE_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getArrowStack() {
        return new ItemStack(ItemInit.ICE_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(this.isInWater())
        {
            BlockPos currentPos = this.getPosition();
            this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.ICEARROWWATER.get(), SoundCategory.PLAYERS, 2f, 1f);
            this.remove();
        }

        if(!this.isAirBorne || !this.isWet() || !this.isInWater())
        {
            BlockPos currentPos = this.getPosition();
            this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.ICEARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (this.inGround) {
            if (!this.isInWater() || !this.isWet()) {
                BlockPos currentPos = this.getPosition();
                this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.ICEARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);

                this.remove();
            }
        }

        if (!this.inGround || !this.isWet() || !this.isInWater()) {
            this.world.addParticle(ParticleTypes.ITEM_SNOWBALL, this.getPosX(), this.getPosY(), this.getPosZ(), 0.0D, 0.0D,
                    0.0D);
            this.world.addParticle(ParticleTypes.ITEM_SNOWBALL, this.getPosX(), this.getPosY(), this.getPosZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected void arrowHit(LivingEntity living) {
        BlockPos currentPos = this.getPosition();
        this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                SoundInit.ICEARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
        living.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 120, 10));
        if(living.equals(EntityType.HUSK)||living.equals(EntityType.MAGMA_CUBE))
        {
            this.setDamage(this.getDamage()*2);
        }

        if(living.equals(EntityType.BLAZE))
        {
            if(!ZConfigManager.getInstance().oneshotblazes())
            {
                this.setDamage(this.getDamage()*2);
            }
        }
        if(living.equals(EntityType.POLAR_BEAR)||living.equals(EntityType.STRAY))
        {
            this.setDamage(this.getDamage()/2);
        }
        super.arrowHit(living);
    }

  //  @Override
 //   public boolean isEntityInsideOpaqueBlock() {
    //    BlockPos currentPos = this.getPosition();
     //   this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
     //           SoundInit.ICEARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
    //    return super.isEntityInsideOpaqueBlock();
//
 //   }

    @Override
    protected void onInsideBlock(BlockState state) {
        BlockState block = world.getBlockState(getPosition());
        if (!this.isAirBorne) {

            if(block != Blocks.COMMAND_BLOCK.getDefaultState() && block != Blocks.BARRIER.getDefaultState() &&
                    block != Blocks.BEDROCK.getDefaultState() && block != Blocks.STRUCTURE_BLOCK.getDefaultState() &&
                    block != Blocks.NETHER_PORTAL.getDefaultState() && block != Blocks.JIGSAW.getDefaultState() &&
                    block != Blocks.END_PORTAL.getDefaultState() && block != Blocks.END_PORTAL_FRAME.getDefaultState() &&
                    block != Blocks.END_GATEWAY.getDefaultState() && block == Blocks.WATER.getDefaultState())
            {
                BlockPos currentPos = this.getPosition();
                this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.ICEARROWWATER.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                if(ZConfigManager.getInstance().icearrowsfrostedice())
                {
                    world.setBlockState(this.getOnPosition(), Blocks.FROSTED_ICE.getDefaultState());
                }
                else world.setBlockState(this.getOnPosition(), Blocks.ICE.getDefaultState());
                this.remove();
            }
            if(block != Blocks.COMMAND_BLOCK.getDefaultState() && block != Blocks.BARRIER.getDefaultState() &&
                    block != Blocks.BEDROCK.getDefaultState() && block != Blocks.STRUCTURE_BLOCK.getDefaultState() &&
                    block != Blocks.NETHER_PORTAL.getDefaultState() && block != Blocks.JIGSAW.getDefaultState() &&
                    block != Blocks.END_PORTAL.getDefaultState() && block != Blocks.END_PORTAL_FRAME.getDefaultState() &&
                    block != Blocks.END_GATEWAY.getDefaultState() && block == Blocks.FIRE.getDefaultState())
            {
                BlockPos currentPos = this.getPosition();
                this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.ICEARROWWATER.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                world.setBlockState(this.getOnPosition(), Blocks.AIR.getDefaultState());
                this.remove();
            }
            if(block != Blocks.COMMAND_BLOCK.getDefaultState() && block != Blocks.BARRIER.getDefaultState() &&
                    block != Blocks.BEDROCK.getDefaultState() && block != Blocks.STRUCTURE_BLOCK.getDefaultState() &&
                    block != Blocks.NETHER_PORTAL.getDefaultState() && block != Blocks.JIGSAW.getDefaultState() &&
                    block != Blocks.END_PORTAL.getDefaultState() && block != Blocks.END_PORTAL_FRAME.getDefaultState() &&
                    block != Blocks.END_GATEWAY.getDefaultState() && block == Blocks.LAVA.getDefaultState())
            {
                BlockPos currentPos = this.getPosition();
                this.world.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.ICEARROWWATER.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                world.setBlockState(this.getOnPosition(), Blocks.COBBLESTONE.getDefaultState());
                this.remove();
            }
        }
    }

    }


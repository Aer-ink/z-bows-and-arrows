package com.ladestitute.zarrowsandstuff.items.food;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

import net.minecraft.item.Item.Properties;

public class SpicyPepperSeedItem extends Item {
    public SpicyPepperSeedItem(Properties properties)
    {
        super(properties.maxStackSize(64));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Spicy pepper seeds. They are hardy, able to thrive in icy climates."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}

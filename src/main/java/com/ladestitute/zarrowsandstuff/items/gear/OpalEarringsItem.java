package com.ladestitute.zarrowsandstuff.items.gear;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

import java.util.List;

public class OpalEarringsItem extends ArmorItem {
        public OpalEarringsItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
            super(materialIn, slot, builder);
        }


    @Override
    public void onArmorTick(ItemStack stack, World world, PlayerEntity player) {
        if(player.isInWater())
        {
            player.addPotionEffect(new EffectInstance(Effects.DOLPHINS_GRACE, 1, 0));
        }
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Earrings sold at Gerudo jewelry shops. They contain opal, a gem that harnesses the power of water to increase your swimming speed."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}


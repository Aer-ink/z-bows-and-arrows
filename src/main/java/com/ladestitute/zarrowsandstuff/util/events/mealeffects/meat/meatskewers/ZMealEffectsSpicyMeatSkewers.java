package com.ladestitute.zarrowsandstuff.util.events.mealeffects.meat.meatskewers;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsSpicyMeatSkewers {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_MEAT_SKEWER.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 4200, 0));
                        h.setColdResistCooldown(4200);
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_MEAT_SKEWER2.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 7200, 0));
                        h.setColdResistCooldown(7200);
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_MEAT_SKEWER3.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 4800, 0));
                        h.setColdResistCooldown(4800);
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_MEAT_SKEWER4.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 10200, 0));
                        h.setColdResistCooldown(10200);
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_MEAT_SKEWER5.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 5400, 0));
                        h.setColdResistCooldown(5400);
                        int health = (int) (player.getHealth() + 18);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_MEAT_SKEWER6.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 7800, 0));
                        h.setColdResistCooldown(7800);
                        int health = (int) (player.getHealth() + 16);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
        }
    }
}


package com.ladestitute.zarrowsandstuff.items.natural;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

import net.minecraft.item.Item.Properties;

public class TopazItem extends Item {
    public TopazItem(Properties properties)
    {
        super(properties.maxStackSize(64));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("This precious yellow gem contains the power of electricity. It's been known to fetch a high price since ancient times."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
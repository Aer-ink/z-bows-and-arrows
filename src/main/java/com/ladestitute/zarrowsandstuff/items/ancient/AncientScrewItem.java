package com.ladestitute.zarrowsandstuff.items.ancient;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

import net.minecraft.item.Item.Properties;

public class AncientScrewItem extends Item {
    public AncientScrewItem(Properties properties)
    {
        super(properties.maxStackSize(64));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A screw used in ancient machinery. It's made of an unknown material, and no matter how many times it's turned, its threads never seem to show signs of wear."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}

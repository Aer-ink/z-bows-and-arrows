package com.ladestitute.zarrowsandstuff.items.arrows;

import com.ladestitute.zarrowsandstuff.entities.arrows.EntityBombArrow;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

import net.minecraft.item.Item.Properties;

public class BombArrowItem extends ArrowItem {
    public BombArrowItem(Properties properties)
    {
        super(properties.maxStackSize(64));
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        EntityBombArrow arrow = new EntityBombArrow(worldIn, shooter);
        return arrow;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A powerful arrow designed to destroy monsters. The explosive powder packed into the tip ignites on impact, dealing massive damage to anything caught in the blast."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
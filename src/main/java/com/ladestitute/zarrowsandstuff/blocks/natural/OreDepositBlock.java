package com.ladestitute.zarrowsandstuff.blocks.natural;

import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import net.minecraft.block.*;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

public class OreDepositBlock extends BaseHorizontalBlock {

    private static final VoxelShape SHAPE = Stream
            .of(Block.makeCuboidShape(5, -0.5, 4.125, 11, 3.5, 10.125),
                    Block.makeCuboidShape(3, 3.5, 5.125, 12, 7.5, 11.125),
                    Block.makeCuboidShape(5, 6.5, 7.125, 9, 10.5, 12.125))
            .reduce((v1, v2) -> {
                return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);
            }).get();


    public OreDepositBlock(AbstractBlock.Properties properties) {
        super(properties);
        runCalculation(SHAPE);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(this).get(state.get(HORIZONTAL_FACING));
    }

    @Override
    public boolean removedByPlayer(BlockState state, World world, BlockPos pos, PlayerEntity player, boolean willHarvest, FluidState fluid) {
        Random rand1 = new Random();
        int oredrop = rand1.nextInt(5);
        ItemStack rubystack = new ItemStack(ItemInit.RUBY.get());
        ItemEntity ruby = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rubystack);
        ItemStack sapphirestack = new ItemStack(ItemInit.SAPPHIRE.get());
        ItemEntity sapphire = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), sapphirestack);
        ItemStack topazstack = new ItemStack(ItemInit.TOPAZ.get());
        ItemEntity topaz = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), topazstack);
        ItemStack opalstack = new ItemStack(ItemInit.OPAL.get());
        ItemEntity opal = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), opalstack);
        ItemStack amberstack = new ItemStack(ItemInit.AMBER.get());
        ItemEntity amber = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), amberstack);
        ItemStack diamondstack = new ItemStack(Items.DIAMOND);
        ItemEntity diamond = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), amberstack);

        if(oredrop == 0)
        {
            world.addEntity(ruby);
        }
        if(oredrop == 1)
        {
            world.addEntity(sapphire);
        }
        if(oredrop == 2)
        {
            world.addEntity(topaz);
        }
        if(oredrop == 3)
        {
            world.addEntity(opal);
        }
        if(oredrop == 4)
        {
            world.addEntity(amber);
        }

        if(ZConfigManager.getInstance().renewableOreDeposits() == true)
        {    world.setBlockState(pos, SpecialBlocksInit.DEPLETED_ORE_DEPOSIT.get().getDefaultState());
        }
        else   world.setBlockState(pos, Blocks.AIR.getDefaultState());
        return false;
    }

    @Override
    public void addInformation (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This deposit contains a good deal of ore. Breaking the rock will yield rock salt, flint, and other materials of varying value."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}

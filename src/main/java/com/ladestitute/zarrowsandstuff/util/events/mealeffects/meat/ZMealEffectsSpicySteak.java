package com.ladestitute.zarrowsandstuff.util.events.mealeffects.meat;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.util.zarrowscap.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsSpicySteak {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();

            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 3600, 0));
                        h.setColdResistCooldown(3600);
                        int health = (int) (player.getHealth() + 6);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK1.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 4200, 0));
                        h.setColdResistCooldown(4200);
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK2.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 4800, 0));
                        h.setColdResistCooldown(4800);
                        int health = (int) (player.getHealth() + 14);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK3.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 5400, 0));
                        h.setColdResistCooldown(5400);
                        int health = (int) (player.getHealth() + 18);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK4.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 6600, 0));
                        h.setColdResistCooldown(6600);
                        int health = (int) (player.getHealth() + 8);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK5.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 9600, 0));
                        h.setColdResistCooldown(9600);
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK6.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 12600, 0));
                        h.setColdResistCooldown(12600);
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK8.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 9600, 0));
                        h.setColdResistCooldown(9600);
                        int health = (int) (player.getHealth() + 10);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PEPPER_STEAK9.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeActivePotionEffect(oppositeresist);
                        player.addPotionEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 12600, 0));
                        h.setColdResistCooldown(12600);
                        int health = (int) (player.getHealth() + 12);
                        if (player.world.isRemote) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
        }
    }
}